<?php
/**
 * 酷站网络
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.keqq.cn
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小虎哥 <1105415366@qq.com>
 * Date: 2018-4-3
 */

namespace app\plugins\controller;

use think\Db;
use app\plugins\logic\CommentLogic;

class Comment extends Base
{
    public $code = 'comment';
    public $listRows = 10; // 评论每页的记录数
    public $replylistRows = 5; // 回复每页的记录数
    public $weappInfo = [];

    public function _initialize() {
        parent::_initialize();
        $this->commentLogic = new CommentLogic;
        // 获取最新的会员信息
        $this->users = session('users');
        $this->users_id = session('users_id');
        // 插件信息
        $this->weappInfo = Db::name('weapp')->field('id')->where(['code'=>$this->code,'status'=>1])->find();
    }

    public function ajax_comment()
    {
        if (empty($this->weappInfo)) {
            $data = [
                'isStatus' => 0,
            ];
            $this->error('评论已禁用', null, $data);
        }

        $aid = input('param.aid/d');
        /*评论回复*/
        $click_like = input('param.click_like/s');
        $param = [];
        $firstRow = input('param.firstRow/d'); // 分页的起始记录数
        if (!empty($firstRow)) {
            $param['firstRow'] = $firstRow;
        }
        $Comment = $this->getCommentReplyData($aid, $click_like, $this->users, $param);
        /* END */
        /*当前登录用户信息*/
        if (!empty($this->users)) {
            $isLogin = $this->users_id;
            $this->users['head_pic'] = get_head_pic($this->users['head_pic']);// 头像处理
        } else {
            $isLogin = 0;
        }
        /* END */
        /*会员配置信息(游客配置是否允许评论：is_comment)*/
        $is_comment = 1;
        if (empty($this->users)) {
            $is_comment = Db::name('weapp_comment_level')->where(['users_level_id'=>0])->getField('is_comment');
            $is_comment  = empty($is_comment) ? 0 : $is_comment;
        }
        /* END */
        /*拼装URL*/
        $ResultUrl = $this->GetUrlData($aid);
        /* END */
        /*追加数据*/
        $plugins = [];
        $plugins['isLogin'] = $isLogin;
        $plugins['isComment'] = $is_comment;
        $plugins['url']     = $ResultUrl;
        $plugins['comment'] = $Comment;
        $plugins['param'] = input('param.');
        /* END */
        $this->assign('plugins', $plugins);
        $this->assign('users', $this->users);

        // 第一页
        if (empty($firstRow)) {
            $html = $this->fetch($this->code.'/'.THEME_STYLE.'/ajax_comment');
        } else { // 分页加载更多
            $html = $this->fetch($this->code.'/'.THEME_STYLE.'/ajax_comment_list');
        }

        $data = [
            'html'  => $html,
            'plugins' => $plugins,
        ];

        $this->success('读取成功！', null, $data);
    }

    // 添加评论回复
    public function ajax_add_comment()
    {
        if (IS_AJAX_POST) {
            // 接收数据
            $param = input('param.');
            // 1.是否允许评论
            // 2.数据判断处理
            // return content
            $content = $this->isComment($param);
            
            /*添加数据*/
            $AddComment = [
                'aid'         => intval($param['aid']),
                'is_review'   => 1, // 默认值为已审核
                'users_id'    => $this->users_id,
                'username'    => $this->users['username'],
                'users_ip'    => clientIP(),
                'content'     => $content,
                'pid'  => !empty($param['comment_id']) ? intval($param['comment_id']) : 0,
                'at_users_id' => !empty($param['at_users_id']) ? intval($param['at_users_id']) : 0,
                'at_comment_id'=> !empty($param['at_comment_id']) ? intval($param['at_comment_id']) : 0,
                'add_time'    => getTime(),
                'update_time' => getTime(),
            ];
            /* END */

            /*评论是否需要审核*/
            $levelData = $this->commentLogic->commentLevel();
            if (!empty($this->users)) {
                if (1 == $levelData['is_review']) $AddComment['is_review'] = 0;
            }else{
                $AddComment['users_id'] = -1;
                $AddComment['username'] = '游客';
                if (1 == $levelData['is_review']) {
                    $AddComment['is_review'] = 0;
                }
            }
            /* END */

            // 添加评论回复
            $insert_id = Db::name('weapp_comment')->add($AddComment);

            // 评论成功
            if (!empty($insert_id)) {
                if (0 == $AddComment['is_review']) {
                    $this->success('评论成功，等待审核中…', null, ['review'=>true]);
                }else{
                    $AddComment['comment_id'] = $insert_id;
                    $AddComment['head_pic']  = handle_subdir_pic($this->users['head_pic']);
                    $ResultData = $this->GetReplyHtml($AddComment);
                    $this->success('评论成功！', null, $ResultData);
                }
            }else{
                $this->error('评论失败，请重试~');
            }
        }
    }

    // 删除评论
    public function ajax_del_comment()
    {
        if (IS_AJAX_POST) {
            // 是否登录
            $this->isLogin();

            /*数据判断*/
            $aid = input('param.aid/d');
            $type = input('param.type/d');
            $comment_id = input('param.comment_id/d');
            if (isset($type) && 1 == $type) {
                // 删除整条回复内容
                $comment_id = input('param.id/d');
            }
            if (empty($comment_id) || empty($aid)) $this->error('请选择删除信息！');
            /* END */

            /*删除条件*/
            $where = [
                'aid' => $aid,
                'comment_id' => $comment_id,
            ];

            // 若操作人为后台管理员则允许删除所有人的评论回答
            if (empty($this->users['admin_id'])) $where['users_id'] = $this->users_id;
            /* END */

            /*执行删除*/
            if (isset($type) && 1 == $type) {
                // 整条评论回答并删除子回答
                $whereOr = [
                    'pid' => $comment_id,
                ];
                // 删除
                $r = Db::name('weapp_comment')->where($where)->whereOr($whereOr)->delete();
            }else{
                // 删除
                $r = Db::name('weapp_comment')->where($where)->delete();
            }
            /* END */

            if (!empty($r)) $this->success('删除成功！');

            $this->error('删除失败！');
        }
    }

    /**
     * 拼装html代码，ajax输出
     */
    private function GetReplyHtml($commentInfo = array())
    {
        $ReplyHtml = '';
        // 如果是需要审核的评论则返回空
        if (empty($commentInfo['is_review'])) return $ReplyHtml;

        /*拼装html代码*/
        // 友好显示时间
        $commentInfo['add_time'] = friend_date($commentInfo['add_time']);
        // 处理内容格式
        $commentInfo['content']  = htmlspecialchars_decode($commentInfo['content']);
        // 用户头像
        if (empty($commentInfo['head_pic']) || -1 == $commentInfo['users_id']) {
            $commentInfo['head_pic'] = ROOT_DIR . '/template/plugins/comment/'.THEME_STYLE.'/skin/images/kong_pic2.png';
        }
        if (!empty($commentInfo['at_users_id'])) {
            $at_userinfo = Db::name('users')->field('nickname,username')->where('users_id', $commentInfo['at_users_id'])->find();
            $at_nickname = empty($at_userinfo['nickname']) ? $at_userinfo['username'] : $at_userinfo['nickname'];
            $commentInfo['content'] = '回复 @'.$at_nickname.':&nbsp;'.$commentInfo['content'];
        }
        // 删除评论回答URL
        $DelCommentUrl = ROOT_DIR."/index.php?m=plugins&c=Comment&a=ajax_del_comment&aid={$commentInfo['aid']}&_ajax=1";

        // 渲染模板
        $this->assign('DelCommentUrl', $DelCommentUrl);
        $this->assign('commentInfo', $commentInfo);
        $ReplyHtml = $this->fetch($this->code.'/'.THEME_STYLE.'/ajax_comment_add');

        // 返回html
        $ReturnHtml = ['review' => false, 'htmlcode' => $ReplyHtml];
        return $ReturnHtml;
    }

    // 审核评论，仅创始人有权限
    public function ajax_review_comment()
    {
        if (IS_AJAX_POST) {
            // 创始人才有权限在前台审核评论
            $admin_info = session('admin_info');
            if (!empty($admin_info) && 0 == $admin_info['parent_id']) {
                $this->isLogin();
                $aid = input('param.aid/d');
                $comment_id = input('param.comment_id/d');
                if (empty($aid) || empty($comment_id)) $this->error('提交信息有误，请刷新重试！');
                // 更新审核评论
                $where = [
                    'aid' => $aid,
                    'comment_id' => $comment_id,
                ];
                $ResultId = Db::name('weapp_comment')->where($where)->update([
                        'is_review' => 1,
                        'update_time' => getTime(),
                    ]);
                if (!empty($ResultId)) $this->success('审核成功！');
                $this->error('审核失败！');
            }else{
                $this->error('没有操作权限！');
            }
        }
    }

    // 编辑评论回复
    public function ajax_edit_comment()
    {
        // 是否登录
        $this->isLogin();
        
        if (IS_AJAX_POST) {
            // 接收数据
            $param = input('param.');
            $aid = input('param.aid/d');
            $comment_id = input('param.comment_id/d');
            // 数据判断处理
            if (empty($comment_id)) $this->error('提交信息有误，请刷新重试！');
            $content = $this->commentLogic->ContentDealWith($param);
            if (empty($content)) $this->error('请写下您的评论！');

            /*更新条件*/
            $where = [
                'comment_id' => $comment_id,
                'aid' => $aid,
            ];
            if (empty($this->users['admin_id'])) $where['users_id'] = $this->users_id;
            /* END */

            // 更新数据
            $updateData = [
                'content'     => $content,
                'users_ip'    => clientIP(),
                'update_time' => getTime(),
            ];
            $ResultId = Db::name('weapp_comment')->where($where)->update($updateData);
            if (!empty($ResultId)) $this->success('编辑成功！');
            $this->error('编辑失败！');
        }

        /*查询评论回复数据*/
        $aid = input('param.aid/d');
        $comment_id = input('param.comment_id/d');
        $where = [
            'a.aid' => $aid,
            'a.comment_id' => $comment_id,
        ];
        if (empty($this->users['admin_id'])) $where['a.users_id'] = $this->users_id;    
        $commentData = Db::name('weapp_comment')->field('a.comment_id, a.aid, a.content, b.title')
            ->alias('a')
            ->join('__ARCHIVES__ b', 'a.aid = b.aid', 'LEFT')   
            ->where($where)
            ->find();
        /* END */

        /*加载数据*/
        $commentData['nickname']  = $this->users['nickname'];
        $plugins = array(
            'comment' => $commentData,
            'EditCommentUrl' => $this->root_dir.'/index.php?m=plugins&c=Comment&a=ajax_edit_comment',
        );
        $this->assign('plugins', $plugins);
        /* END */

        return $this->fetch($this->code.'/'.THEME_STYLE.'/ajax_comment_edit');
    }

    // 获取指定条数的回复(分页)
    public function ajax_page_reply()
    {
        if (IS_AJAX_POST) {
            $aid = input('param.aid/d');
            $comment_id = input('param.comment_id/d');
            $firstRow = input('param.firstRow/d');

            $where = [
                'pid'    => $comment_id,
                'aid'   => $aid,
                'is_review' => 1,
            ];
            $count = Db::name('weapp_comment')->field('comment_id')
                ->where($where)
                ->count();
            $list = Db::name('weapp_comment')->field('*')
                ->where($where)
                ->order('comment_id asc')
                ->limit($firstRow, $this->replylistRows)
                ->select();
            if (!empty($list)) {
                $list = $this->handleReplyData($list);

                // 删除评论回答URL
                $DelCommentUrl = ROOT_DIR."/index.php?m=plugins&c=Comment&a=ajax_del_comment&aid={$aid}&_ajax=1";
                $this->assign('DelCommentUrl', $DelCommentUrl);

                // 渲染模板
                $this->assign('replyList', $list);
                $ReplyHtml = $this->fetch($this->code.'/'.THEME_STYLE.'/ajax_reply_list');

                // 返回数据
                $ismore = 0;
                if (($firstRow + $this->replylistRows) < $count) {
                    $ismore = 1;
                }
                $ResultData = ['review' => false, 'htmlcode' => $ReplyHtml, 'ismore' => $ismore];

                $this->success('查询成功！', null, $ResultData);
            }else{
                $this->error('没有更多数据');
            }
        }
    }

    // 废弃 - 获取指定条数的回复(分页)
/*    public function ajax_page_reply2()
    {
        if (IS_AJAX_POST) {
            $param = input('param.');
            $aid = input('param.aid/d');
            $firstRow = input('param.firstRow/d');
            $listRows = input('param.listRows/d');
            $Comment = $this->getCommentReplyData($aid, null, [], $param);
            if (!empty($Comment['commentData'][0]['commentSubData'])) {
                $ismore = 0;
                $total = $firstRow + $listRows;
                if ($total < $Comment['commentData'][0]['commentSubCount']) {
                    $ismore = 1;
                }
                $ResultData = $this->foreachReplyHtml2($Comment['commentData'][0]['commentSubData'], $ismore);
                $this->success('查询成功！', null, $ResultData);
            }else{
                $this->error('没有更多数据');
            }
        }
    }*/

    // 点赞评论回复
    public function ajax_click_like()
    {
        if (IS_AJAX_POST) {
            // 是否登录
            $this->isLogin();
            // 接收数据
            $aid       = input('param.aid/d');
            $comment_id = input('param.comment_id/d');
            // 数据判断
            if (empty($comment_id) || empty($aid)) $this->error('点赞失败！');

            $where = [
                'aid'       => $aid,
                'comment_id' => $comment_id,
                'users_id'  => $this->users_id,
            ];
            /*查询是否已对评论回复点赞过*/
            $isCount = Db::name('weapp_comment_like')->where($where)->count();
            /* END */

            if (!empty($isCount)) {
                // 已赞过
                $this->error('您已赞过！');
            }else{
                // 添加新的点赞记录
                $saveData = [
                    'aid'   => $aid,
                    'comment_id'   => $comment_id,
                    'users_id'   => $this->users_id,
                    'click_like'  => 1,
                    'users_ip'    => clientIP(),
                    'add_time'    => getTime(),
                    'update_time' => getTime(),
                ];
                $ResultId = Db::name('weapp_comment_like')->add($saveData);
                /* END */

                if (!empty($ResultId)) {
                    /*查询点赞人数更新文档评论表并处理返回*/
                    $ResultData = $this->commentLogic->ClickLikeDealWith($aid, $comment_id, $this->users['nickname']);
                    $this->success('点赞成功！', null, $ResultData);
                    /* END */
                } else {
                    $this->error('点赞失败！');
                }
            }
        }
    }

    // 获取指定条数的回复(分页)
    private function handleReplyData($list = array(), $ismore = 0)
    {
        /*获取与完善列表中相关的用户信息*/
        if (!empty($list)) {
            $users_ids = [];
            foreach ($list as $key => $value) {
                $users_ids[] = $value['users_id'];
                $users_ids[] = $value['at_users_id'];
            }
            $usersRow = Db::name('users')->field('users_id,nickname,username,head_pic')
                ->where(['users_id'=>['IN',$users_ids]])
                ->getAllWithIndex('users_id');

            foreach ($list as $key => $value) {
                // 友好显示时间
                $value['add_time'] = friend_date($value['add_time']);
                // 处理格式
                $value['content']  = htmlspecialchars_decode($value['content']);
                // 处理用户名
                $value['username'] = empty($usersRow[$value['users_id']]['nickname']) ? $usersRow[$value['users_id']]['username'] : $usersRow[$value['users_id']]['nickname'];
                // 处理头像
                $value['head_pic'] = $usersRow[$value['users_id']]['head_pic'];
                if (-1 == $value['users_id']) {
                    $value['username'] = '游客';
                    $value['head_pic'] = $this->root_dir . '/template/plugins/comment/'.THEME_STYLE.'/skin/images/kong_pic2.png';
                }
                $value['head_pic'] = handle_subdir_pic($value['head_pic']);
                // 内容处理
                if (!empty($value['at_users_id'])) {
                    $at_nickname = empty($usersRow[$value['at_users_id']]['nickname']) ? $usersRow[$value['at_users_id']]['username'] : $usersRow[$value['at_users_id']]['nickname'];
                    $value['content'] = '回复 @'.$at_nickname.':&nbsp;'.$value['content'];
                }

                $list[$key] = $value;
            }
        }
        /*end*/

        return $list;
    }

    // 废弃 - 获取指定条数的回复(分页)
    // private function foreachReplyHtml2($data = array(), $ismore = 0)
    // {
    //     $ReplyHtml = '';
    //     foreach ($data as $key => $value) {
    //         // 如果是需要审核的评论则返回空
    //         if (empty($value['is_review'])) return $ReplyHtml;
            
    //         /*拼装html代码*/
    //         if (!empty($value['at_users_id'])) {
    //             $at_userinfo = Db::name('users')->field('nickname,username')->where('users_id', $value['at_users_id'])->find();
    //             $at_nickname = empty($at_userinfo['nickname']) ? $at_userinfo['username'] : $at_userinfo['nickname'];
    //             $value['content'] = '回复 @'.$at_nickname.':&nbsp;'.$value['content'];
    //         }
    //         // 删除评论回答URL
    //         $DelCommentUrl = ROOT_DIR."/index.php?m=plugins&c=Comment&a=ajax_del_comment&aid={$value['aid']}&_ajax=1";

    //         // 渲染模板
    //         $this->assign('DelCommentUrl', $DelCommentUrl);
    //         $this->assign('replyInfo', $value);
    //         $ReplyHtml .= $this->fetch($this->code.'/'.THEME_STYLE.'/ajax_reply_list');
    //     }

    //     // 返回html
    //     $ReturnHtml = ['review' => false, 'htmlcode' => $ReplyHtml, 'ismore' => $ismore];
    //     return $ReturnHtml;
    // }

    // 用户是否登录
    private function isLogin()
    {
        if (empty($this->users_id)) $this->error('请先登录！');
        if (empty($this->weappInfo)) $this->error('评论已禁用');
    }

    // 是否允许评论
    private function isComment($param = [])
    {
        if (empty($this->weappInfo)) $this->error('评论已禁用');

        // 数据判断处理
        if (empty($param['aid'])) $this->error('文档ID不存在，无法继续~');

        if (!empty($this->users)) {
            // 已登录，判断会员组级别是否允许评论
            $row = Db::name('weapp_comment_level')->field('a.is_comment, b.level_name')
                ->alias('a')
                ->join('__USERS_LEVEL__ b', 'a.users_level_id = b.level_id', 'LEFT')
                ->where([
                    'users_level_id'    => $this->users['level'],
                ])
                ->find();
            $is_comment = !empty($row['is_comment']) ? $row['is_comment'] : 0;
            $level_name = !empty($row['level_name']) ? $row['level_name'] : '游客';
        }else{
            // 未登录，判断游客是否允许评论
            $is_comment = Db::name('weapp_comment_level')->where(['users_level_id'=>0])->getField('is_comment');
            $is_comment  = empty($is_comment) ? 0 : $is_comment;
            $level_name = '游客';
        }

        // 不允许评论
        if (empty($is_comment)) $this->error($level_name.'不允许发表评论！');

        $content = $this->commentLogic->ContentDealWith($param);
        if (empty($content)) $this->error('请写下您的评论！');

        return $content;
    }

    // 评论数据
    private function getCommentReplyData($aid = null, $click_like = null, $users = array(), $param = array())
    {
        $aid = intval($aid);

        /*最佳答案*/
        // $bestcomment_id = Db::name('archives')->where('aid', $aid)->getField('bestcomment_id');
        /* END */

        /*查询条件*/
        $commentWhere = ['a.aid' => $aid, 'a.pid' => 0];
        // 不是创始人只允许查看审核的评论
        if (empty($users['admin_id'])) $commentWhere['a.is_review'] = 1;
        /* END */

        /*评论读取条数*/
        $firstRow = !empty($param['firstRow']) ? intval($param['firstRow']) : 0;
        $result['firstRow'] = $firstRow;
        $result['listRows'] = $this->listRows;
        $result['replylistRows'] = $this->replylistRows;
        /* END */
        
        /*排序*/
        $orderway = $click_like;
        if ('desc' != $orderway) {
            $orderway = 'asc';
        }
        $OrderBy = !empty($click_like) ? 'a.click_like '.$orderway.', a.comment_id asc' : 'a.comment_id asc';
        $result['orderway'] = 'desc' == $orderway ? 'asc' : 'desc';
        /* END */

        /*统计评论数*/
        $commentCount = Db::name('weapp_comment')->field('a.comment_id')
            ->alias('a')
            ->where($commentWhere)
            ->count();
        $result['commentCount'] = $commentCount;

        // 评论列表是否显示加载更多事件
        $commentMore = 0;
        if ($firstRow + $this->listRows < $commentCount) {
            $commentMore = 1;
        }
        $result['commentMore'] = $commentMore;

        // 没有评论
        if (empty($commentCount)) return [];

        /*评论*/
        $commentData = Db::name('weapp_comment')->field('a.*, b.head_pic, b.nickname')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->where($commentWhere)
            ->order($OrderBy)
            ->limit($firstRow,$this->listRows)
            ->select();
        /* END */
        $commentIds = get_arr_column($commentData, 'comment_id');

        /*评论对应的回复*/
        $ReplyWhere = [
            'pid|comment_id'   => ['IN', $commentIds],
        ];
        // 不是创始人只允许查看审核的
        if (empty($users['admin_id'])) $ReplyWhere['is_review'] = 1;
        /* END */
        $ReplyRow = Db::name('weapp_comment')->field('*')
            ->where($ReplyWhere)
            ->select();
        // 提取回复用户ID和@用户ID，并获取用户信息
        $ReplyData = $ReplyList = [];
        $usersIdArr = [];
        foreach ($ReplyRow as $key => $value) {
            $usersIdArr[] = $value['users_id'];
            $usersIdArr[] = $value['at_users_id'];
        }
        $usersRow = Db::name('users')->field('users_id,username,nickname,head_pic')
            ->where(['users_id'=>['IN', $usersIdArr]])
            ->getAllWithIndex('users_id');
        foreach ($ReplyRow as $key => $value) {
            // 友好显示时间
            $value['add_time'] = friend_date($value['add_time']);
            // 处理格式
            $value['content']  = htmlspecialchars_decode($value['content']);
            // 头像处理
            $value['head_pic'] = empty($usersRow[$value['users_id']]['head_pic']) ? '' : $usersRow[$value['users_id']]['head_pic'];
            $value['head_pic'] = get_head_pic($value['head_pic']);
            // 用户名
            $value['username'] = empty($usersRow[$value['users_id']]['username']) ? '' : $usersRow[$value['users_id']]['username'];
            // 用户昵称
            $value['nickname'] = empty($usersRow[$value['users_id']]['nickname']) ? $value['username'] : $usersRow[$value['users_id']]['nickname'];
            // @用户名
            $value['at_usersname'] = empty($usersRow[$value['at_users_id']]['username']) ? '' : $usersRow[$value['at_users_id']]['username'];
            // 处理评论者
            if (empty($value['nickname']) && -1 == $value['users_id']) {
                $value['nickname'] = '游客';
                $value['head_pic'] = $this->root_dir . '/template/plugins/comment/'.THEME_STYLE.'/skin/images/kong_pic2.png';
            }
            // 被@的评论者
            if (empty($value['at_usersname']) && -1 == $value['at_users_id']) {
                $value['at_usersname'] = '游客';
            }

            // 每页5条回复
            if (empty($ReplyData[$value['pid']]) || $this->replylistRows > count($ReplyData[$value['pid']])) {
                $ReplyData[$value['pid']][] = $value;
            }
            // 每个评论的所有回复列表
            $ReplyList[$value['pid']][] = $value;
        }
        /*end*/

        /*点赞数据*/
        $commentLikeData = Db::name('weapp_comment_like')->field('a.*, b.username, b.nickname')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->order('like_id desc')
            ->where('comment_id', 'IN', $commentIds)
            ->select();
        $commentLikeData = group_same_key($commentLikeData, 'comment_id');
        /* END */

        foreach ($commentData as $key => $value) {
            // 友好显示时间
            $value['add_time'] = friend_date($value['add_time']);
            // 处理格式
            $value['content']  = htmlspecialchars_decode($value['content']);
            // 头像处理
            $value['head_pic'] = empty($usersRow[$value['users_id']]['head_pic']) ? '' : $usersRow[$value['users_id']]['head_pic'];
            $value['head_pic'] = get_head_pic($value['head_pic']);
            // 用户名
            $value['username'] = empty($usersRow[$value['users_id']]['username']) ? '' : $usersRow[$value['users_id']]['username'];
            // 用户昵称
            $value['nickname'] = empty($usersRow[$value['users_id']]['nickname']) ? $value['username'] : $usersRow[$value['users_id']]['nickname'];
            // 处理评论者
            if (empty($value['nickname']) && -1 == $value['users_id']) {
                $value['nickname'] = '游客';
                $value['head_pic'] = $this->root_dir . '/template/plugins/comment/'.THEME_STYLE.'/skin/images/kong_pic2.png';
            }

            // 第一页回复列表
            $value['commentSubData'] = empty($ReplyData[$value['comment_id']]) ? [] : $ReplyData[$value['comment_id']];

            // 每个评论的回复总数
            $commentSubCount = empty($ReplyList[$value['comment_id']]) ? 0 : count($ReplyList[$value['comment_id']]);
            $value['commentSubCount'] = $commentSubCount;

            // 回复列表是否显示加载更多事件
            $commentSubMore = 0;
            if ($this->replylistRows < $commentSubCount) {
                $commentSubMore = 1;
            }
            $value['commentSubMore'] = $commentSubMore;

            // 点赞列表
            $commentLike = empty($commentLikeData[$value['comment_id']]) ? [] : $commentLikeData[$value['comment_id']];
            $value['commentLike'] = $commentLike;

            /*点赞处理*/
            $like_i = 0;
            $value['commentLike']['LikeNum']  = count($commentLike);
            $value['commentLike']['LikeName'] = '';
            foreach ($commentLike as $LikeKey => $LikeValue) {
                // 获取前三个点赞人处理后退出本次for
                if ($like_i > 2) break;
                // 点赞人用户名\昵称
                $LikeName = empty($LikeValue['nickname']) ? $LikeValue['username'] : $LikeValue['nickname'];
                // 在第二个数据前加入顿号，拼装a链接
                $LikeName = 0 != $like_i ? ' 、'. $LikeName : $LikeName;
                $value['commentLike']['LikeName'] .= $LikeName;
                $like_i++;
            }
            /* END */

            $commentData[$key] = $value;
        }

        $result['commentData'] = $commentData;

        return $result;
    }

    // 评论、回复数据
    private function getCommentReplyData2($aid = null, $click_like = null, $users = array(), $param = array())
    {   
        $aid = intval($aid);
        isset($param['comment_id']) && $param['comment_id'] = intval($param['comment_id']);

        /*最佳答案*/
        // $bestcomment_id = Db::name('archives')->where('aid', $aid)->getField('bestcomment_id');
        /* END */

        /*查询条件*/
        $RepliesWhere = ['a.aid' => $aid, 'a.is_review' => 1];
        $WhereOr = [];
        if (!empty($param['comment_id'])) {
            $RepliesWhere = ['a.comment_id' => $param['comment_id'], 'a.is_review' => 1];
            $WhereOr = ['a.pid' => $param['comment_id']];
        }
        // 若为则创始人则去除仅查询已审核评论这个条件
        if (!empty($users['admin_id'])) unset($RepliesWhere['is_review']);
        /* END */

        /*评论读取条数*/
        $firstRow = !empty($param['firstRow']) ? intval($param['firstRow']) : 0;
        $result['firstRow'] = $firstRow;
        $result['listRows'] = $this->listRows;
        /* END */
        
        /*排序*/
        $orderway = $click_like;
        if ('desc' != $orderway) {
            $orderway = 'asc';
        }
        $OrderBy = !empty($click_like) ? 'a.click_like '.$orderway.', a.comment_id asc' : 'a.comment_id asc';
        $result['orderway'] = 'desc' == $orderway ? 'asc' : 'desc';
        /* END */

        /*统计评论数*/
        $commentCount = Db::name('weapp_comment')->field('a.comment_id')
            ->alias('a')
            ->where($RepliesWhere)
            ->WhereOr($WhereOr)
            ->count();
        $result['commentCount'] = $commentCount;

        // 评论列表是否显示加载更多事件
        $commentMore = 0;
        if ($this->listRows < $commentCount) {
            $commentMore = 1;
        }
        $result['commentMore'] = $commentMore;

        // 没有评论
        if (empty($commentCount)) return [];

        /*评论、回复*/
        $RepliesData = Db::name('weapp_comment')->field('a.*, b.head_pic, b.nickname, c.nickname as `at_usersname`')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->join('__USERS__ c', 'a.at_users_id = c.users_id', 'LEFT')
            ->order($OrderBy)
            ->where($RepliesWhere)
            ->WhereOr($WhereOr)
            // ->limit($firstRow,$this->listRows)
            ->select();
        /* END */

        /*点赞数据*/
        $commentIds = get_arr_column($RepliesData, 'comment_id');
        $commentLikeData = Db::name('weapp_comment_like')->field('a.*, b.nickname')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->order('like_id desc')
            ->where('comment_id', 'IN', $commentIds)
            ->select();
        $commentLikeData = group_same_key($commentLikeData, 'comment_id');
        /* END */

        /*评论处理，拆分回复*/
        $PidData = $commentData = [];
        foreach ($RepliesData as $key => $value) {
            // 友好显示时间
            $value['add_time'] = friend_date($value['add_time']);
            // 处理格式
            $value['content']  = htmlspecialchars_decode($value['content']);
            // 头像处理
            $value['head_pic'] = get_head_pic($value['head_pic']);
            // 处理评论者
            if (empty($value['nickname']) && -1 == $value['users_id']) {
                $value['nickname'] = $value['username'];
                $value['head_pic'] = $this->root_dir . '/template/plugins/comment/'.THEME_STYLE.'/skin/images/kong_pic2.png';
            }
            // 被@的评论者
            if (empty($value['at_usersname']) && -1 == $value['at_users_id']) $value['at_usersname'] = '游客';
            // 是否上一级评论
            if($value['pid'] == 0){
                $PidData[]    = $value;
            }else{
                $commentData[] = $value;
            }
        }
        /* END */

        /*一级评论*/
        foreach($PidData as $key => $PidValue){
            // 一级答案
            $result['commentData'][$key] = $PidValue;
            // 子评论
            $result['commentData'][$key]['commentSubData'] = [];
            // 点赞数据
            $result['commentData'][$key]['commentLike'] = [];

            /*所属子评论处理*/
            foreach($commentData as $commentValue){
                if($commentValue['pid'] == $PidValue['comment_id']){
                    array_push($result['commentData'][$key]['commentSubData'], $commentValue);
                }
            }

            // 每个评论的回复总数
            $commentSubCount = count($result['commentData'][$key]['commentSubData']);
            $result['commentData'][$key]['commentSubCount'] = $commentSubCount;

            // 回复列表是否显示加载更多事件
            $commentSubMore = 0;
            if ($this->listRows < $commentSubCount) {
                $commentSubMore = 1;
            }
            $result['commentData'][$key]['commentSubMore'] = $commentSubMore;
            
            // 读取前五条数据
            $result['commentData'][$key]['commentSubData'] = array_slice($result['commentData'][$key]['commentSubData'], 0, $this->listRows);
            /* END */

            /*点赞处理*/
            $result['commentData'][$key]['commentLike']['LikeNum']  = null;
            $result['commentData'][$key]['commentLike']['LikeName'] = null;
            foreach ($commentLikeData as $LikeKey => $LikeValue) {
                if($PidValue['comment_id'] == $LikeKey){
                    // 点赞总数
                    $LikeNum = count($LikeValue);
                    $result['commentData'][$key]['commentLike']['LikeNum'] = $LikeNum;
                    for ($i=0; $i < $LikeNum; $i++) {
                        // 获取前三个点赞人处理后退出本次for
                        if ($i > 2) break;
                        // 点赞人用户名\昵称
                        $LikeName = $LikeValue[$i]['nickname'];
                        // 在第二个数据前加入顿号，拼装a链接
                        $LikeName = 0 != $i ? ' 、<a>'. $LikeName .'</a>' : '<a>'. $LikeName .'</a>';
                        $result['commentData'][$key]['commentLike']['LikeName'] .= $LikeName;
                    }
                }
            }
            /* END */
        }
        /* END */
        
        /*最佳答案数据*/
        // $result['BestAnswer'] = [];
        // if (!empty($bestcomment_id)) {
        //     foreach ($result['commentData'] as $key => $value) {
        //         if ($bestcomment_id == $value['comment_id']) {
        //             $result['BestAnswer'][$key] = $value;
        //             unset($result['commentData'][$key]);
        //         }
        //     }
        // }
        /* NED */

        return $result;
    }

    private function GetUrlData($aid = null)
    {
        /*评论加载更多的URL*/
        $click_like = input('param.click_like/s');
        $PageCommentUrl = $this->root_dir.'/index.php?m=plugins&c=Comment&a=ajax_comment&aid='.$aid.'&_ajax=1';
        !empty($click_like) && $PageCommentUrl .= "&click_like={$click_like}";
        /*end*/

        $return = [
            // 添加评论的URL
            'AddCommentUrl'  => $this->root_dir.'/index.php?m=plugins&c=Comment&a=ajax_add_comment&aid='.$aid.'&_ajax=1',
            
            // 编辑评论URL
            'EditCommentUrl' => $this->root_dir.'/index.php?m=plugins&c=Comment&a=ajax_edit_comment&aid='.$aid,
            
            // 前台管理员审核评论URL
            'ReviewUrl'     => $this->root_dir.'/index.php?m=plugins&c=Comment&a=ajax_review_comment&aid='.$aid.'&_ajax=1',
            
            // 删除评论URL
            'DelCommentUrl'  => $this->root_dir.'/index.php?m=plugins&c=Comment&a=ajax_del_comment&aid='.$aid.'&_ajax=1',
            
            // 获取指定数量的回复数据（ajax加载更多）
            'PageReplyUrl'=> $this->root_dir.'/index.php?m=plugins&c=Comment&a=ajax_page_reply&aid='.$aid.'&_ajax=1',
            
            // 采纳最佳评论URL
            'BestAnswerUrl' => $this->root_dir.'/index.php?m=plugins&c=Comment&a=ajax_best_answer&aid='.$aid.'&_ajax=1',
            
            // 点赞URL
            'ClickLikeUrl'  => $this->root_dir.'/index.php?m=plugins&c=Comment&a=ajax_click_like&_ajax=1',
            
            // 按点赞量排序
            'commentLikeUrl' => $this->root_dir.'/index.php?m=plugins&c=Comment&a=ajax_comment&aid='.$aid.'&_ajax=1',
            
            // 获取指定数量的回复数据（ajax加载更多）
            'PageCommentUrl'=> $PageCommentUrl,
        ];

        return $return;
    }
}