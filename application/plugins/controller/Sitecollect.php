<?php
/**
 * 酷站网络
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.keqq.cn
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小虎哥 <1105415366@qq.com>
 * Date: 2018-4-3
 */

namespace app\plugins\controller;

class Sitecollect extends Base
{
    /**
     * 构造方法
     */
    public function __construct(){
        parent::__construct();
    }
    /*
     * 获取图片地址
     */
    public function tool(){
        $url =  $_GET['url'];
        $headers = get_headers($url,1);
        if (in_array($headers['Content-Type'],['image/gif','image/jpg','image/jpeg','image/png','image/bmp','image/x-icon','image/webp'])){
            ob_start();
            $context = stream_context_create(
                array('http' => array(
                    'follow_location' => false
                ))
            );
            readfile($url,false,$context);
            $img = ob_get_contents();
            ob_end_clean();

            return $img;
        }

        return '';
    }
}