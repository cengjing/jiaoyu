<?php
/**
 * 酷站网络
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.keqq.cn
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 陈风任 <491085389@qq.com>
 * Date: 2019-7-30
 */

namespace app\plugins\controller;

use think\Db;
use think\Config;
use app\plugins\logic\AskLogic;
use weapp\Ask\model\AskTypeModel;

class Ask extends Base
{   
    public $users = [
        'users_id' => 0,
        'admin_id' => 0,
        'nickname' => '游客',
    ];
    public $users_id = 0;
    public $nickname = '游客';
    public $parent_id = -1;
    public $reward_switch = 0;
    public $reward_type = 'scores';

    public function _initialize() {
        parent::_initialize();

        /*问答本身的URL模式*/
        static $askConfData = null;
        if (null === $askConfData) {
            $askTypeModel = new AskTypeModel;
            $askConfData = $askTypeModel->getWeappData();
        }
        $ask_seo_pseudo = 1;
        if (!empty($askConfData['data']['seo_pseudo'])) {
            $ask_seo_pseudo = intval($askConfData['data']['seo_pseudo']);
        }
        Config::set('ey_config.seo_pseudo', $ask_seo_pseudo);
        /*end*/
        $this->reward_switch = $askConfData['data']['reward_switch'];
        $this->reward_type = $askConfData['data']['reward_type'];
        /*切换伪静态之后，动态URL跳到伪静态问答首页*/
        if ('index' == $this->request->action()) {
            $url = $this->request->url();
            if (3 == $ask_seo_pseudo && stristr($url, '&c=') && stristr($url, '&a=')) {
                $url = url('plugins/Ask/index');
                $this->redirect($url);
            }
        }
        /*end*/

        // 头像处理
        $this->users['head_pic'] = get_head_pic();
        // 问题表
        $this->weapp_ask_db        = Db::name('weapp_ask');
        // 答案表
        $this->weapp_ask_answer_db = Db::name('weapp_ask_answer');
        // 问题回答点赞表
        $this->weapp_ask_answer_like_db = Db::name('weapp_ask_answer_like');
        // 问答业务层
        $this->AskLogic = new AskLogic;
        // 问答数据层
        $this->AskModel = model('Ask');
        // 问答标识
        $this->Code = 'ask/';
        /*获取最新的会员信息*/
        $LatestData = $this->GetUsersLatestData();
        if (!empty($LatestData)) {
            // 会员全部信息
            $this->users = $LatestData;
            // 会员ID
            $this->users_id = $LatestData['users_id'];
            // 会员昵称
            $this->nickname = $LatestData['nickname'];
            // 后台管理员信息
            $parent_id = session('admin_info.parent_id');
            !empty($parent_id) && $this->parent_id = $parent_id;
        } else {
            //过滤不需要登陆的行为
            $ctl_act = CONTROLLER_NAME.'@'.ACTION_NAME;
            $ctl_all = CONTROLLER_NAME.'@*';
            $filter_login_action = [
                'Ask@index',   // 问答内容列表
                'Ask@details', // 问答详情
            ];
            if (!in_array($ctl_act, $filter_login_action) && !in_array($ctl_all, $filter_login_action)) {
                if (IS_AJAX) {
                    if (empty($this->users)) {
                        $this->error('请先登录！');
                    }
                } else {
                    if (isWeixin()) {
                        //微信端
                        $this->redirect('user/Users/users_select_login');
                        exit;
                    }else{
                        // 其他端
                        $this->redirect('user/Users/login');
                        exit;
                    }
                }
            }
        }
        /* END */

        /*加载到模板*/

        $this->assign('users', $this->users);
        $this->assign('nickname', $this->nickname);
        $this->assign('AdminParentId', $this->parent_id);
        /* END */

        /*会员功能是否开启*/
        $logut_redirect_url = '';
        $msg = '';
        $this->usersConfig = getUsersConfigData('all');
        $web_users_switch = tpCache('web.web_users_switch');
        if (empty($web_users_switch) || isset($this->usersConfig['users_open_register']) && $this->usersConfig['users_open_register'] == 1) { 
            // 前台会员中心已关闭
            $logut_redirect_url = ROOT_DIR.'/';
            $msg = '会员中心尚未开启！';
        } else if (session('?users_id') && empty($this->users)) { 
            // 登录的会员被后台删除，立马退出会员中心
            $logut_redirect_url = url('user/Users/centre');
            $msg = '当前用户名不存在！';
        }
        if (!empty($logut_redirect_url)) {
            // 清理session并回到首页
            session('users_id', null);
            session('users', null);
            $this->error($msg, $logut_redirect_url);
            exit;
        }
        /* END */
    }

    /**
     * 问答首页
     */
    public function index()
    {
        $param = input('param.');
        
        // 查询条件处理
        $Where = $this->AskLogic->GetAskWhere($param, $this->parent_id);
        // Url处理
        $UrlData = $this->AskLogic->GetUrlData($param);

        // 最新问题，默认读取20条，可传入条数及字段名称进行获取
        $ResultAsk = $this->AskModel->GetNewAskData($Where);

        // 栏目处理
        $TypeData = $this->AskModel->GetAskTypeData($param);

        // 热门帖子，周榜
        $WeekList = $this->AskModel->GetAskWeekListData();

        // 热门帖子，总榜
        $TotalList = $this->AskModel->GetAskTotalListData();

        // 主页SEO信息
        $type_id = !empty($param['type_id']) ? intval($param['type_id']) : 0;
        $SeoData = $this->AskLogic->GetSeoData($type_id);

        // 数组合并加载到模板
        $result = array_merge($ResultAsk, $UrlData, $TypeData, $WeekList, $TotalList, $SeoData);

        $result['conf']['reward_switch'] = $this->reward_switch;
        $result['conf']['reward_type'] = $this->reward_type;
        $eyou = array(
            'field' => $result,
        );

        $this->assign('eyou', $eyou);
        return $this->fetch($this->Code.THEME_STYLE.'/index');
    }

    /**
     * 问题详情页
     */
    public function details()
    {
        $param = input('param.');

        if (empty($param['ask_id'])) $this->error('请选择浏览的问题');

        // 增加问题浏览点击量
        $this->AskModel->UpdateAskClick($param['ask_id']);

        // 问题详情数据
        $AskDetails = $this->AskModel->GetAskDetailsData($param, $this->parent_id, $this->users_id);
        if (0 == $AskDetails['code']) $this->error($AskDetails['msg']);

        // 问题回答数据，包含最佳答案
        $AskReplyData = $this->AskModel->GetAskReplyData($param, $this->parent_id);

        // 栏目处理
        $TypeData = $this->AskModel->GetAskTypeData($param);

        // 热门帖子，周榜
        $WeekList = $this->AskModel->GetAskWeekListData();

        // 热门帖子，总榜
        $TotalList = $this->AskModel->GetAskTotalListData();

        // Url处理
        $UrlData = $this->AskLogic->GetUrlData($param);

        // 数组合并加载到模板
        $result = array_merge($AskDetails, $AskReplyData, $TypeData, $WeekList, $TotalList, $UrlData);
        
        $eyou = array(
            'field' => $result,
        );
        $this->assign('eyou', $eyou);
        return $this->fetch($this->Code.THEME_STYLE.'/details');
    }

    // 提交问题
    public function add_ask()
    {
        if (IS_AJAX_POST || IS_POST) {
            $param = input('param.');
            // 是否登录、是否允许发布问题、数据判断及处理，返回内容数据
            $content = $this->ParamDealWith($param);
            $param['title'] = $this->check_sensitive($param['title']);
            /*添加数据*/
            $AddAsk = [
                'type_id'     => $param['ask_type_id'],
                'users_id'    => $this->users_id,
                'ask_title'   => $param['title'],
                'content'     => $content,
                'users_ip'    => clientIP(),
                'add_time'    => getTime(),
                'update_time' => getTime(),
                'money' => !empty($param['money']) ? $param['money'] : 0,
            ];
            // 如果这个会员组属于需要审核的，则追加
            if (1 == $this->users['ask_is_review']) $AddAsk['is_review'] = 0;
            /* END */

            $ResultId = $this->weapp_ask_db->add($AddAsk);
            if (!empty($ResultId)) {
                if (0 < $param['money'] && $this->reward_switch == 1){
                    //悬赏 扣余额/积分并插入记录
                    $this->AskModel->setUb($this->reward_type,$this->users_id,$ResultId,$param['money']);
                }

                $url = $this->AskLogic->GetUrlData($param, 'NewDateUrl');
                if (1 == $this->users['ask_is_review']) {
                    $this->success('回答成功，但你的回答需要管理员审核！', $url, ['review' => true]);
                }else{
                    $this->success('发布成功！', $url);
                }
            }else{
                $this->error('发布的信息有误，请检查！');
            }
        }

        // 是否允许发布问题
        $this->IsRelease();

        // 栏目处理
        $result = $this->AskModel->GetAskTypeData(null, 'add_ask');
        $result['SubmitAddAsk'] = $this->AskLogic->GetUrlData(null, 'SubmitAddAsk');
        $eyou = array(
            'field' => $result,
        );

        $eyou['conf']['reward_switch'] = $this->reward_switch;
        $eyou['conf']['reward_type'] = $this->reward_type;
        if (1 == $eyou['conf']['reward_switch']){
            $eyou['field']['reward_money'] = Db::name('users')->where('users_id',$this->users_id)->value($eyou['conf']['reward_type']);
            $eyou['field']['reward_desc'] = $eyou['conf']['reward_type'] ==  'scores' ? '积分' : '元';
        }

        $this->assign('eyou', $eyou);

        return $this->fetch($this->Code.THEME_STYLE.'/add_ask');
    }

    // 编辑问题
    public function edit_ask()
    {
        if (IS_AJAX_POST || IS_POST) {
            $param = input('param.');
            // 是否登录、是否允许发布问题、数据判断及处理，返回内容数据
            $content = $this->ParamDealWith($param, false);
            $param['title'] = $this->check_sensitive($param['title']);
            /*添加数据*/
            $UpAsk = [
                'type_id'     => $param['ask_type_id'],
                'ask_title'   => $param['title'],
                'content'     => $content,
                'users_ip'    => clientIP(),
                'update_time' => getTime(),
            ];
            if (!empty($param['money'])) $UpAsk['money'] = $param['money'];
            // 如果这个会员组属于需要审核的，则追加
            if (1 == $this->users['ask_is_review']) $UpAsk['is_review'] = 0;
            /* END */

            /*条件处理*/
            $where['ask_id'] = $param['ask_id'];

            $ori_money = $this->weapp_ask_db->where('ask_id',$param['ask_id'])->getField('money');
            if ($ori_money > $param['money']){
                $msg = '悬赏积分不能少于'.$ori_money.'！';
                if ($this->reward_type ==  'users_money'){
                    $msg = '悬赏金额不能小于'.$ori_money.'元！';
                }
                $this->error($msg);
            }

            // 不是后台管理则只能修改自己的问题
            if (empty($this->users['admin_id'])) $where['users_id'] = $this->users_id;    
            /* END */

            $ResultId = $this->weapp_ask_db->where($where)->update($UpAsk);
            if (!empty($ResultId)) {
                if ($ori_money < $param['money'] && $this->reward_switch == 1){
                    $this->AskModel->setUb($this->reward_type,$this->users_id,$param['ask_id'],$param['money'],'edit',$ori_money);
                }
                $url = $this->AskLogic->GetUrlData($param, 'AskDetailsUrl');
                $this->success('编辑成功！', $url);
            }else{
                $this->error('编辑的信息有误，请检查！');
            }
        }

        // 是否允许发布问题
        $this->IsRelease(false);
        $ask_id = input('ask_id/d');

        $where['ask_id'] = $ask_id;
        // 不是后台管理则只能修改自己的问题
        if (empty($this->users['admin_id'])) $where['users_id'] = $this->users_id;    
        $Info = $this->weapp_ask_db->where($where)->find();
        if (empty($Info)) $this->error('请选择编辑的问题！');

        // 栏目处理
        $result = $this->AskModel->GetAskTypeData($Info, 'edit_ask');
        $result['Info'] = $Info;
        $result['EditAskUrl'] = $this->AskLogic->GetUrlData(['ask_id'=>$ask_id], 'EditAskUrl');

        $eyou = array(
            'field' => $result,
        );

        $eyou['conf']['reward_switch'] = $this->reward_switch;
        $eyou['conf']['reward_type'] = $this->reward_type;
        if (1 == $eyou['conf']['reward_switch']){
            $eyou['field']['reward_money'] = Db::name('users')->where('users_id',$this->users_id)->value($eyou['conf']['reward_type']);
            $eyou['field']['reward_desc'] = $eyou['conf']['reward_type'] ==  'scores' ? '积分' : '元';
        }

        $this->assign('eyou', $eyou);

        return $this->fetch($this->Code.THEME_STYLE.'/edit_ask');
    }

    // 会员中心--我的问答--我的问题\回复
    public function ask_index()
    {
        $method = input('param.method');
        // 加载会员中心所需数据
        $this->AssignData(input('param.'));
        /*数据查询处理*/
        if (!empty($method)) {
            // 回答数据
            $result['Method'] = $method;
            $ResultData = $this->AskModel->GetUsersAskDataNew($this->users_id, false,10);
        }else{
            // 提问数据
            $result['Method'] = '';
            $ResultData = $this->AskModel->GetUsersAskDataNew($this->users_id, true,10);
        }
        $eyou = array(
            'field' => $ResultData,
        );
        /* END */
        $this->assign('eyou', $eyou);
        
        $viewfile = 'ask_index';
        $usersTplVersion = getUsersTplVersion();
        if ($usersTplVersion != 'v1') {
            $viewfile .= '_'.$usersTplVersion;
        }
        $this->assign('usersTplVersion', $usersTplVersion);
        //会员中心相关信息begin
        $php_servicemeal = tpCache('php.php_servicemeal');
        $this->assign('php_servicemeal', $php_servicemeal);
        // 未读消息数
        $unread_num = Db::name('users')->where(['users_id' => $this->users_id])->value("unread_notice_num");
        $this->assign('unread_num',$unread_num);
        //会员中心相关信息end
        return $this->fetch($this->Code.THEME_STYLE.'/'.$viewfile);
    }

    // 用户首页--我的问题--我的回答(暂时停用)
    public function users_index()
    {
        // 是否传入用户ID
        $view_uid = input('view_uid/d');
        if (empty($view_uid)) $this->error('访问的会员不存在！');

        // 用户是否存在
        $result = $this->AskModel->GetUsersAskCount($view_uid);
        if (empty($result)) $this->error('访问的会员不存在！');

        // 不为空则默认回答页
        $method = input('param.method');
        if (!empty($method)) {
            $result['Method']  = $method;
            $ResultData = $this->AskModel->GetUsersAskData($view_uid, false,10);
        }else{
            $result['Method']  = '';
            $ResultData = $this->AskModel->GetUsersAskData($view_uid, true,10);
        }
        if (!empty($ResultData)) {
            $result['AskData'] = $ResultData['AskData'];
            $result['pageStr'] = $ResultData['pageStr'];
        }

        $eyou = array(
            'field' => $result,
        );
        $this->assign('eyou', $eyou);
        return $this->fetch($this->Code.THEME_STYLE.'/users_index');
    }

    // 采纳最佳答案
    public function ajax_best_answer()
    {
        if (IS_AJAX_POST) {
            if (!empty($this->users['admin_id']) || $this->users_id = input('post.users_id/d')) {
                $param = input('param.');

                // 数据判断处理
                if (empty($param['answer_id']) || empty($param['ask_id']) ) $this->error('请选择采纳的回答！');

                // 更新问题数据表
                $Updata = [
                    'ask_id'        => $param['ask_id'],
                    'status'        => 1,
                    'solve_time'    => getTime(),
                    'bestanswer_id' => $param['answer_id'],
                    'update_time'   => getTime(),
                ];
                $ResultId = $this->weapp_ask_db->update($Updata);

                if (!empty($ResultId)) {
                    $money = $this->weapp_ask_db->where('ask_id',$param['ask_id'])->value('money');
                    if (0 < $money && $this->reward_switch == 1){
                        $answer_user_id = $this->weapp_ask_answer_db->where('answer_id', $param['answer_id'])->value('users_id');
                        //悬赏金额插入记录
                        $this->AskModel->setUb($this->reward_type,$answer_user_id,$param['ask_id'],$money,'answer',0,$param['answer_id']);
                    }

                    // 将这个问题下的所有答案设置为非最佳答案
                    $this->weapp_ask_answer_db->where('ask_id', $param['ask_id'])->update(['is_bestanswer'=>0]);
                    // 设置当前问题为最佳答案
                    $this->weapp_ask_answer_db->where('answer_id', $param['answer_id'])->update(['is_bestanswer'=>1]);
                    $this->success('已采纳！');
                }else{
                    $this->error('请选择采纳的回答！');
                }
            }else{
                $this->error('无操作权限！');
            }
        }
    }

    // 添加回答
    public function ajax_add_answer()
    {
        if (IS_AJAX_POST || IS_POST) {
            $param = input('param.');
            // 是否登录、是否允许发布问题、数据判断及处理，返回内容数据
            $content = $this->AnswerDealWith($param, true);

            /*添加数据*/
            $AddAnswer = [
                'ask_id'      => $param['ask_id'],
                // 如果这个会员组属于需要审核的，则追加。 默认1为已审核
                'is_review'   => 1 == $this->users['ask_is_review'] ? 0 : 1,
                'type_id'     => $param['type_id'],
                'users_id'    => $this->users_id,
                'username'    => $this->users['username'],
                'users_ip'    => clientIP(),
                'content'     => $content,
                // 若是回答答案则追加数据
                'answer_pid'  => !empty($param['answer_id']) ? $param['answer_id'] : 0,
                // 用户则追加数据
                'at_users_id' => !empty($param['at_users_id']) ? $param['at_users_id'] : 0,
                'at_answer_id'=> !empty($param['at_answer_id']) ? $param['at_answer_id'] : 0,
                'add_time'    => getTime(),
                'update_time' => getTime(),
            ];
            $ResultId = $this->weapp_ask_answer_db->add($AddAnswer);
            /* END */

            if (!empty($ResultId)) {
                // 增加问题回复数
                $this->AskModel->UpdateAskReplies($param['ask_id'], true);
                if (1 == $this->users['ask_is_review']) {
                    $this->success('回答成功，但你的回答需要管理员审核！', null, ['review' => true]);
                }else{
                    $AddAnswer['answer_id'] = $ResultId;
                    $AddAnswer['head_pic']  = $this->users['head_pic'];
                    $AddAnswer['at_usersname'] = '';
                    if (!empty($AddAnswer['at_users_id'])) {
                        $FindData = Db::name('users')->field('nickname, username')->where('users_id', $AddAnswer['at_users_id'])->find();
                        $AddAnswer['at_usersname'] = empty($FindData['nickname']) ? $FindData['username'] : $FindData['nickname'];
                    }
                    $ResultData = $this->AskLogic->GetReplyHtml($AddAnswer);
                    $this->success('回答成功！', null, $ResultData);
                }
            }else{
                $this->error('提交信息有误，请刷新重试！');
            }
        }
    }

    // 编辑回答
    public function ajax_edit_answer()
    {
        if (IS_AJAX_POST || IS_POST) {
            $param = input('param.');
            // 是否登录、是否允许发布问题、数据判断及处理，返回内容数据
            $content = $this->AnswerDealWith($param, false);

            /*编辑数据*/
            $UpAnswerData = [
                'content'     => $content,
                'users_ip'    => clientIP(),
                'update_time' => getTime(),
            ];
            // 如果这个会员组属于需要审核的，则追加
            if (1 == $this->users['ask_is_review']) $UpAnswerData['is_review'] = 0;
            /* END */

            // 更新条件
            $where = [
                'answer_id' => $param['answer_id'],
                'ask_id'    => $param['ask_id'],
            ];
            if (empty($this->users['admin_id'])) $where['users_id'] = $this->users_id;   
            
            // 更新数据
            $ResultId = $this->weapp_ask_answer_db->where($where)->update($UpAnswerData);
            if (!empty($ResultId)) {
                $url = $this->AskLogic->GetUrlData($param, 'AskDetailsUrl');
                if (1 == $this->users['ask_is_review']) {
                    $this->success('编辑成功，但你的回答需要管理员审核！', $url, ['review' => true]);
                }else{
                    $this->success('编辑成功！', $url);
                }
            } else {
                $this->error('编辑的信息有误，请检查！');
            }
        }

        // 是否允许发布问题
        $this->IsAnswer(false);

        $answer_id = input('param.answer_id/d');
        $where = [
            'a.answer_id' => $answer_id,
        ];
        if (empty($this->users['admin_id'])) {
            $where['a.users_id'] = $this->users_id;    
        }
        $AnswerData = $this->weapp_ask_answer_db->field('a.answer_id, a.ask_id, a.content, b.ask_title')
            ->alias('a')
            ->join('__WEAPP_ASK__ b', 'a.ask_id = b.ask_id', 'LEFT')
            ->where($where)
            ->find();
        if (empty($AnswerData)) $this->error('要修改的回答不存在！');

        // 更新人
        $AnswerData['nickname'] = $this->nickname;
        $result['Info'] = $AnswerData;
        $result['EditAnswerUrl'] = $this->AskLogic->GetUrlData(['answer_id'=>$answer_id], 'EditAnswer');
        $eyou = array(
            'field' => $result,
        );
        $this->assign('eyou', $eyou);
        return $this->fetch($this->Code.THEME_STYLE.'/edit_answer');
    }

    // 删除问题、回答
    public function ajax_del_answer()
    {
        if (IS_AJAX_POST) {
            // 是否登录
            $this->UsersIsLogin();

            /*数据判断*/
            $type = input('param.type/d');
            $ask_id = input('param.ask_id/d');
            $answer_id = input('param.answer_id/d');
            if (isset($type) && 3 == $type) {
                // 删除整条回复内容
                $answer_id = input('param.id/d');
            }else if (isset($type) && 1 == $type) {
                // 删除整个提问问题则执行
                $this->DelAsk(input('param.'));
            }
            if (empty($answer_id) || empty($ask_id)) $this->error('请选择删除信息！');
            /* END */

            /*删除条件*/
            $Where = [
                'ask_id'    => $ask_id,
                'answer_id' => $answer_id,
            ];
            // 若操作人为后台管理员则允许删除所有人的评论回答
            if (empty($this->users['admin_id'])) $where['users_id'] = $this->users_id;
            /* END */

            /*执行删除*/
            if (isset($type) && 3 == $type) {
                // 整条评论回答，算计整个评论有多少条数量
                $CountWhere['answer_pid'] = $answer_id;
                // 查询整条回答的评论数量
                $DelNum = $this->weapp_ask_answer_db->where($Where)->whereOr($CountWhere)->count();
                // 删除
                $ResultId = $this->weapp_ask_answer_db->where($Where)->whereOr($CountWhere)->delete();
            }else{
                $DelNum = 0;
                // 删除
                $ResultId = $this->weapp_ask_answer_db->where($Where)->delete();
            }
            /* END */

            if (!empty($ResultId)) {
                // 减少问题回复数
                $this->AskModel->UpdateAskReplies($ask_id, false, $DelNum);
                $this->success('删除成功！');
            }else{
                $this->error('删除信息错误，请刷新重试！');
            }
        }
    }

    // 删除整个提问问题
    public function DelAsk($param = array())
    {
        if (empty($param['ask_id']) || empty($param['id'])) $this->error('请选择要删除的提问问题！');
        if (empty($this->users['admin_id'])) $this->error('无操作权限！');

        /*执行删除*/
        $Where['ask_id'] = $param['ask_id'];
        $ResultId = $this->weapp_ask_db->where($Where)->delete();
        if (!empty($ResultId)) {
            // 同步删除对应问题下所有回答
            $this->weapp_ask_answer_db->where($Where)->delete();
            // 同步删除对应问题下所有点赞
            $this->weapp_ask_answer_like_db->where($Where)->delete();
            $url = $this->AskLogic->GetUrlData($param, 'NewDateUrl');
            $this->success('删除成功！', $url);
        }else{
            $this->error('删除信息错误，请刷新重试！');
        }
        /* END */
    }

    // 点赞
    public function ajax_click_like()
    {
        if (IS_AJAX_POST) {
            // 是否登录
            $this->UsersIsLogin();

            $ask_id    = input('param.ask_id/d');
            $answer_id = input('param.answer_id/d');
            if (empty($answer_id) || empty($ask_id)) $this->error('请选择点赞信息！');

            $Where = [
                'ask_id'    => $ask_id,
                'users_id'  => $this->users_id,
                'answer_id' => $answer_id,
            ];
            $IsCount = $this->weapp_ask_answer_like_db->where($Where)->count();
            if (!empty($IsCount)) {
                $this->error('您已赞过！');
            }else{
                // 添加新的点赞记录
                $AddData = [
                    'click_like'  => 1,
                    'users_ip'    => clientIP(),
                    'add_time'    => getTime(),
                    'update_time' => getTime(),
                ];
                $AddData = array_merge($Where, $AddData);
                $ResultId = $this->weapp_ask_answer_like_db->add($AddData);

                if (!empty($ResultId)) {
                    unset($Where['users_id']);
                    $LikeCount = $this->weapp_ask_answer_like_db->where($Where)->count();
                    if (1 == $LikeCount) {
                        $LikeName = '<a href="javascript:void(0);">'.$this->nickname.'</a>';
                    }else{
                        $LikeName = '<a href="javascript:void(0);">'.$this->nickname.'</a>、 ';
                    }
                    $data = [
                        // 点赞数
                        'LikeCount' => $LikeCount,
                        // 点赞人
                        'LikeName'  => $LikeName,
                    ];

                    // 同步点赞次数到答案表
                    $UpdataNew = [
                        'click_like'  => $LikeCount,
                        'update_time' => getTime(),
                    ];
                    $this->weapp_ask_answer_db->where($Where)->update($UpdataNew);
                    $this->success('点赞成功！', null, $data);
                }
            }
        }
    }

    // 审核问题，仅创始人有权限
    public function ajax_review_ask()
    {
        if (IS_AJAX_POST) {
            // 创始人才有权限在前台审核评论
            if (0 == $this->parent_id) {
                $this->UsersIsLogin();
                $param = input('param.');
                if (empty($param['ask_id'])) $this->error('请选择需要审核的问题！');
                // 更新审核问题
                $UpAakData = [
                    'is_review' => 1,
                    'update_time' => getTime(),
                ];
                $ResultId = $this->weapp_ask_db->where('ask_id', $param['ask_id'])->update($UpAakData);
                if (!empty($ResultId)) $this->success('审核成功！');
                $this->error('审核失败！');
            }else{
                $this->error('没有操作权限！');
            }
        }
    }

    // 审核评论，仅管理员有权限
    public function ajax_review_comment()
    {
        if (IS_AJAX_POST) {
            // 创始人才有权限在前台审核评论
            if (0 == $this->parent_id) {
                $this->UsersIsLogin();
                $param = input('param.');
                if (empty($param['ask_id']) || empty($param['answer_id'])) $this->error('提交信息有误，请刷新重试！');
                // 更新审核评论
                $where = [
                    'ask_id'    => $param['ask_id'],
                    'answer_id' => $param['answer_id'],
                ];
                $UpAnswerData = [
                    'is_review' => 1,
                    'update_time' => getTime(),
                ];
                $ResultId = $this->weapp_ask_answer_db->where($where)->update($UpAnswerData);
                if (!empty($ResultId)) $this->success('审核成功！');
                $this->error('审核失败！');
            }else{
                $this->error('没有操作权限！');
            }
        }
    }

    // 获取指定数量的评论数据（分页）
    public function ajax_show_comment()
    {
        if (IS_AJAX_POST) {
            $param = input('param.');
            $Comment = $this->AskModel->GetAskReplyData($param, $this->parent_id);
            $Data = !empty($param['is_comment']) ? $Comment['AnswerData'][0]['AnswerSubData'] : $Comment['BestAnswer'][0]['AnswerSubData'];
            if (!empty($Data)) {
                $ResultData = $this->AskLogic->ForeachReplyHtml($Data, $this->parent_id);
                if (empty($ResultData['htmlcode'])) $this->error('没有更多数据');
                $this->success('查询成功！', null, $ResultData);
            }else{
                $this->error('没有更多数据');
            }
        }
    }

    // 加载会员中心所需数据
    private function AssignData($param = array())
    {
        // 是否登录
        $this->UsersIsLogin();

        // 主题颜色
        $this->assign('usersConfig', $this->usersConfig);
        $this->usersConfig['theme_color'] = $theme_color = !empty($this->usersConfig['theme_color']) ? $this->usersConfig['theme_color'] : '#ff6565'; // 默认主题颜色
        $this->assign('theme_color', $theme_color);

        // 是否为手机端
        $is_mobile = isMobile() ? 1 : 2;
        $this->assign('is_mobile',$is_mobile);
        
        // 是否为端微信
        $is_wechat = isWeixin() ? 1 : 2;
        $this->assign('is_wechat',$is_wechat);

        // 是否为微信端小程序
        $is_wechat_applets = isWeixinApplets() ? 1 : 0;
        $this->assign('is_wechat_applets',$is_wechat_applets);

        // 焦点
        $Focus = empty($param['method']) ? 1 : 2;
        $this->assign('Focus',$Focus);

        // 菜单名称
        $this->MenuTitle = Db::name('users_menu')->where([
                'mca'   => 'plugins/Ask/ask_index',
                'lang'  => $this->home_lang,
            ])->getField('title');
        $this->assign('MenuTitle',$this->MenuTitle);
    }

    // 是否登录
    private function UsersIsLogin()
    {
        if (empty($this->users) || empty($this->users_id)) $this->error('请先登录！');
    }

    // 是否允许发布、编辑问题
    private function IsRelease($is_add = true)
    {
        if (empty($this->users['ask_is_release'])) {
            if (!empty($is_add)) {
                $this->error($this->users['level_name'].'不允许发布问题！');
            }else{
                $this->error($this->users['level_name'].'不允许编辑问题！');
            }
        }
    }

    // 问题数据判断及处理，返回问题内容数据
    private function ParamDealWith($param = [], $is_add = true)
    {
        //token验证
        $this->checkToken($param);
        // 是否登录
        $this->UsersIsLogin();
        // 是否允许该发布、编辑
        $this->IsRelease($is_add);
        //限制提交频率
        $rate = $this->AskLogic->GetRateData();
        if (!empty($rate['is_open']) && !empty($rate['duration'])){
            $map = [
                'users_id'    => $this->users_id,
                'update_time'  => array('gt', getTime() - $rate['duration']),
            ];
            $have = $this->weapp_ask_db->where($map)->find();
            if($have){
                $this->error('您提交得太频繁了，请歇会再发布！');
            }
        }

        if($is_add){
            //统计提问次数
            $count = $this->weapp_ask_db->where(['users_id'=>$this->users_id])->whereTime('add_time','today')->count();
            if ($count >= $this->users['ask_count']) {
                $this->error("【{$this->users['level_name']}】每天最多提问{$this->users['ask_count']}次");
            }
        }
        /*数据判断*/
        $content = '';
        if (empty($param)) $this->error('请提交完整信息！');
        if (empty($param['title'])) $this->error('请填写问题标题！');
        if (empty($param['ask_type_id'])) $this->error('请选择问题分类！');
        $content = $this->AskLogic->ContentDealWith($param);
        if (empty($content)) $this->error('请填写问题描述！');
        // 编辑时执行判断
        if (empty($is_add) && empty($param['ask_id'])) $this->error('请确认编辑问题！');
        /* END */
        //敏感词过滤规则
        $content = $this->check_sensitive($content);

        return $content;
    }
    //敏感词过滤
    private function check_sensitive($content){
        $sensitive = $this->AskLogic->GetSensitiveData();
        if (!empty($sensitive['sensitive_switch']) && !empty($sensitive['sensitive_data'])){
            list($count,$sensitiveWord,$content) = $this->AskLogic->sensitive($sensitive['sensitive_data'],$content);
            if ($count > 0  && !empty($sensitiveWord)){     //存在敏感词
                switch ($sensitive['sensitive_switch']){
                    case "1":   //替换敏感词为 *** ， 已经替换过，不需要做其他任何处理
                        break;
                    case "2":   //进入审核
                        $this->users['ask_is_review'] = 1;
                        break;
                    case "3":   //禁止发帖
                        $this->users['is_lock'] = -1;
                        session('users',$this->users);
                        Db::name("users")->where(['users_id'=>$this->users_id])->update(["is_lock"=>-1]);
                        $this->error('您发布的内容存在敏感词，暂停发布信息权限！');
                        break;
                    case "4":   //自动拉黑用户
                        $this->users['is_lock'] = -99;
                        session('users',$this->users);
                        Db::name("users")->where(['users_id'=>$this->users_id])->update(["is_lock"=>-99]);
                        $this->error('您发布的内容存在敏感词！');
                        break;
                }
            }
        }

        return $content;
    }
    // 是否允许发布、编辑评论回复
    private function IsAnswer($is_add = true)
    {
        if (empty($this->users['ask_is_release'])) {
            if (!empty($is_add)) {
                $this->error($this->users['level_name'].'不允许回复答案！');
            }else{
                $this->error($this->users['level_name'].'不允许编辑答案！');
            }
        }
    }
    //token验证
    private function checkToken($param){
        // token数据验证
        $token = '__token__';
        foreach ($param as $key => $val) {
            if (preg_match('/^__token__/i', $key)) {
                $token = $key;
                break;
            }
        }
        $rule = [
            'form_id'    => 'require|token:'.$token,
        ];
        $message = [
            'form_id.require' => '数据验证错误，请重新提交',
        ];
        $validate = new \think\Validate($rule, $message);
        if(!$validate->batch()->check($param))
        {
            $error = $validate->getError();
            $error_msg = array_values($error);
            $this->error($error_msg[0]);
        }
    }
    // 评论回复数据处理，返回评论回复内容数据
    private function AnswerDealWith($param = [], $is_add = true)
    {
        //token验证
        $this->checkToken($param);
        // 是否登录
        $this->UsersIsLogin();
        // 是否允许发布、编辑
        $this->IsAnswer($is_add);
        //限制提交频率
        $rate = $this->AskLogic->GetRateData();
        if (!empty($rate['is_open']) && !empty($rate['duration'])){
            $map = [
                'users_id'    => $this->users_id,
                'update_time'  => array('gt', getTime() - $rate['duration']),
            ];
            $have = $this->weapp_ask_answer_db->where($map)->find();
            if($have){
                $this->error('您提交得太频繁了，请歇会再发布！');
            }
        }
        if (!empty($is_add)) {    /*数据判断*/
            //统计回答次数
            $count = Db::name('weapp_ask_answer')->where(['users_id'=>$this->users_id])->whereTime('add_time','today')->count();
            if ($count >= $this->users['answer_count']) {
                $this->error("【{$this->users['level_name']}】每天最多回答{$this->users['answer_count']}次");
            }
            // 添加时执行判断
            if (empty($param) || empty($param['ask_id']) ) $this->error('提交信息有误，请刷新重试！');
        }else{
            // 编辑时执行判断
            if (empty($is_add) && empty($param['ask_id'])) $this->error('请确认编辑问题！');
        }

        $content = '';
        $content = $this->AskLogic->ContentDealWith($param);
        if (empty($content)) $this->error('请写下你的回答！');
        /* END */
        $content = $this->check_sensitive($content);

        return $content;
    }

    /**
     * 获取登录的会员最新数据
     */
    private function GetUsersLatestData($users_id = null) {
        $users_id = empty($users_id) ? session('users_id') : $users_id;
        if(!empty($users_id)) {
            /*读取的字段*/
            $field = 'a.*, b.*, b.discount as level_discount';
            /* END */
            /*查询数据*/
            $users = \think\Db::name('users')->field($field)
                ->alias('a')
                ->join('__USERS_LEVEL__ b', 'a.level = b.level_id', 'LEFT')
                ->where([
                    'a.users_id'        => $users_id,
                    'a.lang'            => get_home_lang(),
                    'a.is_activation'   => 1,
                    'a.is_del'          => 0,
                ])->find();
            // 会员不存在则返回空
            if (empty($users)) return false;
            /* END */

            $ask_users_level_lsit = Db::name('weapp_ask_users_level')->where(['level_id'=>$users['level_id']])->find();
            $users['ask_count'] = !empty($ask_users_level_lsit['ask_count']) ? intval($ask_users_level_lsit['ask_count']) : 5;
            $users['answer_count'] = !empty($ask_users_level_lsit['answer_count']) ? intval($ask_users_level_lsit['answer_count']) : 5;
            $users['ask_is_release'] = !empty($ask_users_level_lsit['ask_is_release']) ? intval($ask_users_level_lsit['ask_is_release']) : 0;
            $users['ask_is_review'] = !empty($ask_users_level_lsit['ask_is_review']) ? intval($ask_users_level_lsit['ask_is_review']) : 0;

            /*会员数据处理*/
            // 头像处理
            $users['head_pic'] = get_head_pic($users['head_pic']);
            // 昵称处理
            $users['nickname'] = empty($users['nickname']) ? $users['username'] : $users['nickname'];
            // 密码为空并且存在openid则表示微信注册登录，密码字段更新为0，可重置密码一次。
            $users['password'] = empty($users['password']) && !empty($users['open_id']) ? 0 : 1;
            // 删除登录密码及支付密码
            unset($users['paypwd']);
            // 级别处理
            $LevelData = [];
            if (intval($users['level_maturity_days']) >= 36600) {
                $users['maturity_code'] = 1;
                $users['maturity_date'] = '终身';
            }else if (0 == $users['open_level_time'] && 0 == $users['level_maturity_days']) {
                $users['maturity_code'] = 0;
                $users['maturity_date'] = '';// 没有升级会员，置空
            }else{
                /*计算剩余天数*/
                $days = $users['open_level_time'] + ($users['level_maturity_days'] * 86400);
                // 取整
                $days = ceil(($days - getTime()) / 86400);
                if (0 >= $days) {
                    /*更新会员的级别*/
                    $LevelData = model('EyouUsers')->UpUsersLevelData($users_id);
                    /* END */
                    $users['maturity_code'] = 2;
                    $users['maturity_date'] = '';// 会员过期，置空
                }else{
                    $users['maturity_code'] = 3;
                    $users['maturity_date'] = $days.' 天';
                }
                /* END */
            }
            /* END */
            
            // 合并数据
            $LatestData = array_merge($users, $LevelData);
            /*更新session*/
            session('users',    $LatestData);
            // session('open_id',  $LatestData['open_id']);
            session('users_id', $LatestData['users_id']);
            setcookie('users_id', $LatestData['users_id'], null);
            /* END */
            // 返回数据
            return $LatestData;
        }else{
            // session中不存在会员ID则返回空
            return false;
        }
    }
    public function ajax_list()
    {
        if (IS_AJAX_POST){
            $param = input('param.');
            $page = $param['page'];

            // 查询条件处理
            $where = $this->AskLogic->GetAskWhere($param, $this->parent_id);

            $return = $this->AskModel->GetAjaxAskPage($page,$where);
            $return['conf']['reward_switch'] = $this->reward_switch;
            $return['conf']['reward_type'] = $this->reward_type;
            $this->assign('eyou',$return);

            $html = $this->fetch($this->Code.THEME_STYLE.'/page');

            $return['html'] = $html;
            $this->success('success','',$return);
        }

    }
}