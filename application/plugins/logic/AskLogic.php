<?php
/**
 * 酷站网络
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.keqq.cn
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 陈风任 <491085389@qq.com>
 * Date: 2019-7-30
 */

namespace app\plugins\logic;

use think\Db;
use app\plugins\model\AskModel;

/**
 * 逻辑定义
 * Class CatsLogic
 * @package plugins\Logic
 */
class AskLogic
{
    // 查询条件处理
    public function GetAskWhere($param = array(), $parent_id = null)
    {
    	// 查询条件
        $where = [
        	// 0未解决，1已解决
            'a.status' 	  => ['IN',[0, 1]],
            // 问题是否审核，1是，0否
	    	'a.is_review' => 1,
        ];

        // 创始人可以看到所有数据，包括未审核问题
        if (0 === $parent_id) unset($where['a.is_review']);

        // 查询指定栏目分类下的问题
        if (!empty($param['type_id'])) $where['a.type_id'] = $param['type_id'];

        // 不为空则表示查询--悬赏--待回答问题
        if (!empty($param['is_recom']) && 3 == intval($param['is_recom'])) $where['a.money']  = ['>',0];

        // 不为空则表示查询待回答问题
        if (!empty($param['is_recom']) && 2 == intval($param['is_recom'])) $where['a.replies']  = 0;
        
        // 推荐问题
        if (!empty($param['is_recom']) && 1 == intval($param['is_recom'])) $where['a.is_recom'] = 1;
        
        // 搜索问题
        if (!empty($param['search_name'])) {
        	$where['a.ask_title'] = ['LIKE', "%{$param['search_name']}%"];
        }
        
        return $where;
    }

    // Url处理
    public function GetUrlData($param = array(), $SpecifyUrl = null)
    {
        if (empty($param['ask_id'])) $param['ask_id'] = 0;
    	$result = [];
    	// 最新问题url
        $result['NewDateUrl'] = url('plugins/Ask/index');
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=index';
        
        // 问题详情页url
        $result['AskDetailsUrl'] = url('plugins/Ask/details', ['ask_id'=>$param['ask_id']]);
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=details&ask_id='.$param['ask_id'];

        // 推荐问题url
        $result['RecomDateUrl'] = url('plugins/Ask/index', ['type_id'=>0, 'is_recom'=>1]);
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=index&is_recom=1';
        // 悬赏问题列表url
        $result['RewardUrl'] = url('plugins/Ask/index', ['type_id'=>0, 'is_recom'=>3]);
        
        // 等待回答url
        $result['PendingAnswerUrl'] = url('plugins/Ask/index', ['type_id'=>0, 'is_recom'=>2]);
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=index&is_recom=2';
        
        // 提交回答url
        $result['AddAnswerUrl'] = url('plugins/Ask/ajax_add_answer', ['ask_id'=>$param['ask_id'], '_ajax'=>1], true, false, 1, 1, 0);
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=ajax_add_answer&ask_id='.$param['ask_id'];
        
        // 删除回答url
        $result['DelAnswerUrl'] = url('plugins/Ask/ajax_del_answer', ['ask_id'=>$param['ask_id'], '_ajax'=>1], true, false, 1, 1, 0);
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=ajax_del_answer&ask_id='.$param['ask_id'];
        
        // 点赞回答url
        $result['ClickLikeUrl'] = url('plugins/Ask/ajax_click_like', ['_ajax'=>1], true, false, 1, 1, 0);
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=ajax_click_like';
        
		// 发布问题url
		$result['AddAskUrl'] = url('plugins/Ask/add_ask');
        // 提交问题url
        $result['SubmitAddAsk'] = url('plugins/Ask/add_ask', ['_ajax'=>1], true, false, 1, 1, 0);
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=add_ask';
        
		// 编辑问题url
		$result['EditAskUrl'] = url('plugins/Ask/edit_ask', ['ask_id'=>$param['ask_id']]);
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=edit_ask&ask_id='.$param['ask_id'];
        
		// 用户问题首页
		$result['UsersIndexUrl'] = url('plugins/Ask/ask_index');
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=ask_index';
        
		// 编辑回答url
		$result['EditAnswer'] = url('plugins/Ask/ajax_edit_answer');
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=ajax_edit_answer';
        if ('ajax_edit_answer' == request()->action()) {
            $result['EditAnswer'] = url('plugins/Ask/ajax_edit_answer', ['_ajax'=>1], true, false, 1, 1, 0);
        }

		// 采纳最佳答案url
		$result['BestAnswerUrl'] = url('plugins/Ask/ajax_best_answer', ['ask_id'=>$param['ask_id'], '_ajax'=>1], true, false, 1, 1, 0);
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=ajax_best_answer&ask_id='.$param['ask_id'];
        
        // 获取指定数量的评论数据（分页）
        $result['ShowCommentUrl'] = url('plugins/Ask/ajax_show_comment', ['ask_id'=>$param['ask_id'], '_ajax'=>1], true, false, 1, 1, 0);
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=ajax_show_comment&ask_id='.$param['ask_id'].'&_ajax=1';

        // 创始人审核评论URL(前台)
        $result['ReviewCommentUrl'] = url('plugins/Ask/ajax_review_comment', ['ask_id'=>$param['ask_id'], '_ajax'=>1], true, false, 1, 1, 0);
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=ajax_review_comment&ask_id='.$param['ask_id'].'&_ajax=1';

        // 创始人审核问题URL(前台)
        $result['ReviewAskUrl'] = url('plugins/Ask/ajax_review_ask', ['_ajax'=>1], true, false, 1, 1, 0);
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=ajax_review_ask&_ajax=1';

		// 按点赞量排序url
		$result['AnswerLikeNum'] = url('plugins/Ask/details', ['ask_id' => $param['ask_id']], true, false, 1, 1, 0);
        
        // 等待回答url
        if (!empty($param['type_id'])) {
            $result['PendingAnswerUrl'] = url('plugins/Ask/index', ['type_id'=>$param['type_id'], 'is_recom'=>2]);
            // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=index&type_id='.$param['type_id'].'&is_recom=2';
        }

        if (!empty($SpecifyUrl)) {
            if (!empty($result[$SpecifyUrl])) {
                return $result[$SpecifyUrl];
            }else{
                return $result['NewDateUrl'];
            }
        }else{
            return $result;
        }

    }

    // 关键词标红
    public function GetRedKeyWord($SearchName, $ask_title)
    {
        $ks = explode(' ',$SearchName);
        foreach($ks as $k){
            $k = trim($k);
            if($k == '') continue;
            if(ord($k[0]) > 0x80 && strlen($k) < 1) continue;
            $ask_title = str_replace($k, "<font color='red'>$k</font>", $ask_title);
        }
        return $ask_title;
    }

    // 内容转义处理
    public function ContentDealWith($param = null)
    {
    	if (!empty($param['content'])) {
            $content = $param['content'];
        }else if(!empty($param['ask_content'])){
            $content = $param['ask_content'];
        }else{
        	return false;
        }

    	// 斜杆转义
        $content = addslashes($content);
        // 过滤内容的style属性
        $content = preg_replace('/style(\s*)=(\s*)[\'|\"](.*?)[\'|\"]/i', '', $content);
        // 过滤内容的class属性
        $content = preg_replace('/class(\s*)=(\s*)[\'|\"](.*?)[\'|\"]/i', '', $content);

        return $content;
    }

    // 栏目分类格式化输出
    public function GetTypeHtmlCode($PidData = array(), $TidData = array(), $type_id = null)
    {
    	// 下拉框拼装
    	$HtmlCode = '<select name="ask_type_id" id="ask_type_id" class="tit_input" style="width: unset;">';
        $HtmlCode .= '<option value="0">请选择分类</option>';
    	foreach ($PidData as $P_key => $PidValue) {
    		/*是否默认选中*/
    		$selected = '';
    		if ($type_id == $PidValue['type_id']) $selected = 'selected';
    		/* END */

    		/*一级下拉框*/
    		$HtmlCode .= '<option value="'.$PidValue['type_id'].'" '.$selected.'>'.$PidValue['type_name'].'</option>';
    		/* END */

    		foreach ($TidData as $T_key => $TidValue) {
    			if ($TidValue['parent_id'] == $PidValue['type_id']) {
    				/*是否默认选中*/
    				$selected = '';
    				if ($type_id == $TidValue['type_id']) $selected = 'selected';
    				/* END */

    				/*二级下拉框*/
    				$HtmlCode .= '<option value="'.$TidValue['type_id'].'" '.$selected.'>&nbsp; &nbsp; &nbsp;'.$TidValue['type_name'].'</option>';
    				/* END */
    			}
    		}
    	}
    	$HtmlCode .= '</select>';
    	return $HtmlCode;
    }

    // 拼装html代码
    public function GetReplyHtml($data = array())
    {
        $ReplyHtml = '';
        // 如果是需要审核的评论则返回空
        if (empty($data['is_review'])) return $ReplyHtml;
        
        /*拼装html代码*/
        // 友好显示时间
        $data['add_time'] = friend_date($data['add_time']);
        // 处理内容格式
        $data['content']  = htmlspecialchars_decode($data['content']);
        if (!empty($data['at_users_id'])) {
            $data['content'] = '回复 @'.$data['at_usersname'].':&nbsp;'.$data['content'];
        }
        // 删除评论回答URL
        $DelAnswerUrl = $this->GetUrlData($data, 'DelAnswerUrl');

        // 拼装html
        $ReplyHtml = <<<EOF
<li class="secend-li" id="{$data['answer_id']}_answer_li">
    <div class="head-secend">
        <a><img src="{$data['head_pic']}" style="width:30px;height:30px;border-radius:100%;margin-right: 16px;"></a>
        <strong>{$data['username']}</strong>
        <span style="margin:0 10px"> | </span>
        <span>{$data['add_time']}</span>
        <div style="flex-grow:1"></div>
        <span id="{$data['answer_id']}_replyA" onclick="replyUser('{$data['answer_pid']}','{$data['users_id']}','{$data['username']}','{$data['answer_id']}')" class="secend-huifu-btn" style="cursor: pointer;">回复</span>
        &nbsp;&nbsp;|&nbsp;&nbsp;
        <a data-url="{$DelAnswerUrl}" onclick="DataDel(this, '{$data['answer_id']}', 2)" class="secend-huifu-btn" style="cursor: pointer; color:red;">删除</a>
    </div>
    <div class="secend-huifu-text">
        {$data['content']}
    </div>
</li>
EOF;
    // 返回html
    $ReturnHtml = ['review' => false, 'htmlcode' => $ReplyHtml];
    return $ReturnHtml;
    }

    // 获取指定条数的评论(分页)
    public function ForeachReplyHtml($data = array(), $parent_id = null)
    {
        $ReplyHtml = '';
        foreach ($data as $key => $value) {
            // 如果是需要审核的评论则返回空
            $review = '';
            if (empty($value['is_review']) && 0 == $parent_id) {
                // 创始人审核评论URL(前台)
                $ReviewCommentUrl = $this->GetUrlData($value, 'ReviewCommentUrl');
                $review = <<<EOF
<span id='{$value['answer_id']}_Review'>
    <span data-url='{$ReviewCommentUrl}' onclick="Review(this, '{$value['answer_id']}')" class="secend-huifu-btn" style="cursor: pointer; color: red;" title="该评论未审核，可点击审核，仅创始人可操作">审核</span>
    <span style="margin:0 10px"> | </span>
</span>
EOF;
            } else if (empty($value['is_review'])) {
                // 其他人查询数据，去除未审核评论，跳过这条数据拼装
                unset($value); continue;
            }

            /*拼装html代码*/
            if (!empty($value['at_users_id'])) {
                $value['content'] = '回复 @'.$value['at_usersname'].':&nbsp;'.$value['content'];
            }

            // 删除评论回答URL
            $DelAnswerUrl = $this->GetUrlData($value, 'DelAnswerUrl');
            // 拼装html
            $ReplyHtml .= <<<EOF
<li class="secend-li" id="{$value['answer_id']}_answer_li">
    <div class="head-secend">
        <a><img src="{$value['head_pic']}" style="width:30px;height:30px;border-radius:100%;margin-right: 16px;"></a>
        <strong>{$value['username']}</strong>
        <span style="margin:0 10px"> | </span>
        <span>{$value['add_time']}</span>
        <div style="flex-grow:1"></div>
        {$review}
        <span id="{$value['answer_id']}_replyA" onclick="replyUser('{$value['answer_pid']}','{$value['users_id']}','{$value['username']}','{$value['answer_id']}')" class="secend-huifu-btn" style="cursor: pointer;">回复</span>
        &nbsp;&nbsp;|&nbsp;&nbsp;
        <a data-url="{$DelAnswerUrl}" onclick="DataDel(this, '{$value['answer_id']}', 2)" class="secend-huifu-btn" style="cursor: pointer; color:red;">删除</a>
    </div>
    <div class="secend-huifu-text">
        {$value['content']}
    </div>
</li>
EOF;
        }

    // 返回html
    $ReturnHtml = ['review' => false, 'htmlcode' => $ReplyHtml];
    return $ReturnHtml;
    }

    /**
     * 获取SEO信息
     * @param string $inc_type [description]
     */
    public function GetSeoData($type_id = 0)
    {
        $inc_type = !empty($type_id) ? 'lists' : 'index';

        $seoInfo = [
            'seo_title' => '问答中心',
            'seo_keywords' => '',
            'seo_description' => '',
        ];
        $typename = Db::name('arctype')->where(['weapp_code'=>'Ask'])->getField('typename');
        !empty($typename) && $seoInfo['seo_title'] = $typename;

        $data = Db::name('weapp')->where(['code'=>'Ask'])->getField('data');
        $dataArr = unserialize($data);
        if ('index' == $inc_type && !empty($dataArr['seo_info'][$inc_type])) {
            $seoInfo = $dataArr['seo_info'][$inc_type];
        } else {
            if ('lists' == $inc_type) {
                $p = input('param.p/d');
                $result = Db::name('weapp_ask_type')->field('*')->where(['type_id'=>$type_id])->find();
                if (!empty($result['seo_title'])) {
                    $seoInfo['seo_title'] = $result['seo_title'] . ($p > 1 ? "_第{$p}页" : '');
                } else {
                    $seoInfo['seo_title'] = $result['type_name'] . ' - ' . $typename . ($p > 1 ? "_第{$p}页" : '');
                }
                !empty($result['seo_keywords']) && $seoInfo['seo_keywords'] = $result['seo_keywords'];
                !empty($result['seo_description']) && $seoInfo['seo_description'] = $result['seo_description'];
            }
        }

        return $seoInfo;
    }
    /*
     * 获取限制提交频率
     */
    public function GetRateData(){
        $rate = [];
        $data = Db::name('weapp')->where(['code'=>'Ask'])->getField('data');
        $dataArr = unserialize($data);
        !empty($dataArr['rate']) && $rate = $dataArr['rate'];

        return $rate;
    }
    /*
     * 获取敏感词过滤设置信息
     */
    public function GetSensitiveData(){
        $sensitive = [];
        $data = Db::name('weapp')->where(['code'=>'Ask'])->getField('data');
        $dataArr = unserialize($data);
        !empty($dataArr['sensitive']) && $sensitive = $dataArr['sensitive'];
        if (!empty($sensitive['sensitive_data'])){
            $sensitive['sensitive_data'] = str_replace("，",",",$sensitive['sensitive_data']);
            $sensitive['sensitive_data'] = explode(",",$sensitive['sensitive_data']);
        }

        return $sensitive;
    }
    /*
     * 判断是否存在敏感词
     * @paramarray $list 定义敏感词一维数组
     * @paramstring $string 要过滤的内容
     * @returnstring $log 处理结果[敏感词个数，敏感词内容，替换后的字符串]
     */
    public function sensitive($list, $string){
        $count = 0; //违规词的个数
        $sensitiveWord = ''; //违规词
        $stringAfter = $string; //替换后的内容
        $pattern = "/" . implode("|", $list) . "/i"; //定义正则表达式
        if (preg_match_all($pattern, $string, $matches)) { //匹配到了结果
            $patternList = $matches[0]; //匹配到的数组
            $count = count($patternList);
            $sensitiveWord = implode(',', $patternList); //敏感词数组转字符串
            $replaceArray = array_combine($patternList, array_fill(0, count($patternList), '**')); //把匹配到的数组进行合并，替换使用
            $stringAfter = strtr($string, $replaceArray); //结果替换
        }

        return [$count,$sensitiveWord,$stringAfter];

    }

}