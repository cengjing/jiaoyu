<?php
/**
 * 酷站网络
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.keqq.cn
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小虎哥 <1105415366@qq.com>
 * Date: 2018-4-3
 */

namespace app\plugins\logic;

use think\Db;

/**
 * 逻辑定义
 * Class CatsLogic
 * @package plugins\Logic
 */
class CommentLogic
{
    /**
     * 构造函数
     */
    public function __construct(){

    }

    /**
     * 评论级别权限
     */
    public function commentLevel()
    {
        $UsersData = session('users');
        if (!empty($UsersData)) {
            // 已登录，判断会员组级别是否允许评论
            $row = Db::name('weapp_comment_level')->field('a.is_comment, a.is_review, b.level_name')
                ->alias('a')
                ->join('__USERS_LEVEL__ b', 'a.users_level_id = b.level_id', 'LEFT')
                ->where([
                    'users_level_id'    => $UsersData['level'],
                ])
                ->find();
        }else{
            // 未登录，判断游客是否允许评论
            $row = Db::name('weapp_comment_level')->field('is_comment,is_review')->where(['users_level_id'=>0])->find();
        }
        $is_comment = isset($row['is_comment']) ? $row['is_comment'] : 0;
        $is_review = isset($row['is_review']) ? $row['is_review'] : 1;
        $level_name = isset($row['level_name']) ? $row['level_name'] : '游客';

        $data = [
            'is_comment'    => $is_comment,
            'is_review'    => $is_review,
            'level_name'    => $level_name,
        ];

        return $data;
    }

    /**
     * 内容过滤
     */
    public function ContentDealWith($param = array())
    {
        $content = '';

        if (!empty($param['content'])) {
            $content = $param['content'];
        } else if (!empty($param['plugins_comment_content'])) {
            $content = $param['plugins_comment_content'];
        }

        if (!empty($content)) {
            // 斜杆转义
            $content = addslashes($content);
            // 过滤内容的style属性
            $content = preg_replace('/style(\s*)=(\s*)[\'|\"](.*?)[\'|\"]/i', '', $content);
            // 过滤内容的class属性
            $content = preg_replace('/class(\s*)=(\s*)[\'|\"](.*?)[\'|\"]/i', '', $content);
        }

        return $content;
    }

    /**
     * 点赞数记录与统计
     */
    public function ClickLikeDealWith($aid, $comment_id, $nickname)
    {
        $Where = [
            'aid'   => $aid,
            'comment_id'   => $comment_id,
        ];
        $LikeCount = Db::name('weapp_comment_like')->where($Where)->count();
        if (1 == $LikeCount) {
            $LikeName = $nickname;
        }else{
            $LikeName = $nickname.'、 ';
        }
        $ResultData = [
            // 点赞数
            'LikeCount' => $LikeCount,
            // 点赞人
            'LikeName'  => $LikeName,
        ];

        /*同步点赞次数到答案表*/
        $UpdataNew = [
            'click_like'  => $LikeCount,
            'update_time' => getTime(),
        ];
        Db::name('weapp_comment')->where([
                'aid'   => $aid,
                'comment_id'   => $comment_id,
            ])->update($UpdataNew);
        /* END */

        return $ResultData;
    }
}