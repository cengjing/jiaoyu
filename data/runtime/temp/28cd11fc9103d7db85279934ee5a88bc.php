<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:41:"./template/mobile/users/users_welcome.htm";i:1648540172;s:59:"/data/www/jiaoyu/template/mobile/users/skin/css/diy_css.htm";i:1648540172;}*/ ?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <title>会员中心-<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_name"); echo $__VALUE__; ?></title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
        <link href="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_cmspath"); echo $__VALUE__; ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />

        <!-- 新样式 2020-12-8 -->
        <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/element/index.css","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/userfont/iconfont.css","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/e-user.css","","",""); echo $__VALUE__; ?>
        
<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style>
  /*
  <?php echo $theme_color; ?>
  <?php echo hex2rgba($theme_color,0.8); ?> */
    a:hover {
        color: <?php echo $theme_color; ?>;
    }
    .ey-container .ey-header-top {
        background: linear-gradient(90deg, <?php echo $theme_color; ?> 10%, <?php echo hex2rgba($theme_color,0.8); ?> 60%);
    }
    .ey-container .ey-con .oper-row .el-button.active {
        background-color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .el-button--primary {
        background-color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .el-button--primary.is-plain {
        color: <?php echo $theme_color; ?>;
        border-color: <?php echo hex2rgba($theme_color,0.2); ?>;
        background-color:<?php echo hex2rgba($theme_color,0.1); ?>;
    }
    .el-button--primary.is-plain:focus, .el-button--primary.is-plain:hover {
        background: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .el-button--primary:hover, .el-button--primary:focus {
        background: <?php echo hex2rgba($theme_color,0.9); ?>;
        border-color: <?php echo hex2rgba($theme_color,0.9); ?>;
        color: #fff;
    }
    .el-input.is-active .el-input__inner, .el-input__inner:focus {
        border-color:<?php echo $theme_color; ?>;
    }
    .ey-container .radio-label .radio:checked+.check-mark {
        border: 1px solid <?php echo $theme_color; ?>;
        background-color: <?php echo $theme_color; ?>;
    }
    .ey-container .radio-label .radio:checked+.check-mark:after {
        background:<?php echo $theme_color; ?>;
    }
    .ey-container .item-from-flex .item-con .link a {
        color:<?php echo $theme_color; ?>;
    }
    .iphone_z {
        border: 1px solid <?php echo $theme_color; ?>;
        color: <?php echo $theme_color; ?>;
    }
    .email_z {
        border: 1px solid <?php echo $theme_color; ?>;
        color: <?php echo $theme_color; ?>;
    }
    .ey-container .item-from-flex .item-con .checkbox-label .checkbox:checked+.check-mark {
        border: 1px solid <?php echo $theme_color; ?>;
        background-color: <?php echo $theme_color; ?>;
    }
    .ey-container .item-from-flex .item-con .radio-label .radio:checked+.check-mark {
        border: 1px solid <?php echo $theme_color; ?>;
        background-color: <?php echo $theme_color; ?>;
    }
    .ey-container .address-add {
        background-color:<?php echo $theme_color; ?>;
    }
    .ey-container .address-con .address-item.cur {
        border: 1px solid <?php echo hex2rgba($theme_color,0.6); ?>;
    }
    .ey-container .address-con .address-item.cur:before {
        color:<?php echo $theme_color; ?>;
        border-top: 50px solid <?php echo $theme_color; ?>;
    }
    .ey-container div.dataTables_paginate .paginate_button.active>a, .ey-containerdiv.dataTables_paginate .paginate_button.active>a:focus, .ey-containerdiv.dataTables_paginate .paginate_button.active>a:hover {
        background: <?php echo $theme_color; ?> !important;
        border-color: <?php echo $theme_color; ?> !important;
    }
    .ey-container .pagination>li>a, .ey-container .pagination>li>span {
        color: <?php echo $theme_color; ?>;
    }
    .btn-primary{
        background-color: <?php echo $theme_color; ?> !important;
    }
    .list_z .re_ed {
        color:<?php echo $theme_color; ?>;
    }
    .btn_zb{
        background-color: <?php echo $theme_color; ?> !important;
    }
    .el-button--danger {
        background-color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>
    }
    .my-top {
        background: <?php echo hex2rgba($theme_color,0.1); ?>;
    }
    .m-user-title {
        color: <?php echo $theme_color; ?>;
    }
    .recharge .pc-vip-list.active {
        border: <?php echo $theme_color; ?> 1px solid;
        background-color:<?php echo hex2rgba($theme_color,0.1); ?>;
    }
    .recharge .pc-vip-list .icon-recomd {
        background: <?php echo $theme_color; ?>;
    }
    .recharge .pc-vip-list .pc-vip-name {
        color: <?php echo $theme_color; ?>;
    }
    .recharge .pc-vip-list .money {
        color: <?php echo $theme_color; ?>;
    }
    .recharge .pc-vip-list .money em {
        color: <?php echo $theme_color; ?>;
    }
    .button2 {
        background: <?php echo $theme_color; ?>;
    }
    .el-button:focus, .el-button:hover {
        color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
        background-color: <?php echo hex2rgba($theme_color,0.1); ?>;
    }
    .ey-container .checkbox-label .checkbox:checked+.check-mark {
        background-color: <?php echo $theme_color; ?>;
        border: 1px solid <?php echo $theme_color; ?>;
    }
    .ey-container .checkbox-label .checkbox:checked+.check-mark:after {
        background:<?php echo $theme_color; ?>;
    }
    .ey-container .pay-type .payTag a {
        color: <?php echo $theme_color; ?>;
        border-bottom: 1px solid <?php echo $theme_color; ?>;
    }
    .ey-container .column-title .column-name {
        color:<?php echo $theme_color; ?>;
    }
    .ey-container .pay-type .pay-con .pay-type-item.active:before {
        color: <?php echo $theme_color; ?>;
        border-bottom: 30px solid <?php echo $theme_color; ?>;
    }
    .ey-container .pay-type .pay-con .pay-type-item.active {
        border-color: <?php echo hex2rgba($theme_color,0.4); ?>;
    }
    .ey-popups .switchCheck :checked + i {
        background: <?php echo $theme_color; ?>;   
    }
    
</style>

<script type="text/javascript">
    var __root_dir__ = "";
    var __version__ = "<?php echo $version; ?>";
    var __lang__ = "<?php echo $home_lang; ?>";
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
</script>

        <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/static/common/js/jquery.min.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/js/bootstrap.min.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/layer-v3.1.0/layer.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/js/global.js","","",""); echo $__VALUE__; ?>

    </head>
    <body>
        <div class="ey-container">
            <div class="ey-header">
                <div class="ey-header-top">
                    <div class="user-info">
                        <div class="user-info-top">
                            <div class="user-info-l face"><a href="<?php echo url("user/Users/info","",true,false,null,null,null);?>"><img id="head_pic_a" src="<?php echo get_head_pic($users['head_pic']); ?>"/></a></div>
                            <div class="user-info-m">
                                <div class="name"><?php echo $users['username']; switch($users['level']): case "1": ?> <span class="yey-user-list-vip1"></span> <?php break; case "2": ?> <span class="yey-user-list-vip2"></span> <?php break; case "3": ?> <span class="yey-user-list-vip3"></span>  <?php break; endswitch; ?></div>
                                <div class="level">
                                        <span><?php echo $users['level_name']; ?></span>&emsp;&emsp;<?php if($users['level'] != '1'): ?> 剩余会员天数：<span class="red"><?php echo $users['maturity_date']; ?></span> <?php endif; ?>
                                </div>
                            </div>
                            <div class="user-info-r"></div>
                        </div>
                        <!-- 信息统计 start -->
                        <div class="user-info-bottom">
                            <a href="<?php echo url("user/Users/score_index","",true,false,null,null,null);?>" class="user-info-item">
                                <span id="users_scores"><?php echo $users['scores']; ?></span>
                                <span><?php echo $score_name; ?></span>
                            </a>
                            <a href="<?php echo url("user/Pay/pay_consumer_details","",true,false,null,null,null);?>" class="user-info-item">
                                <span><?php echo $users['users_money']; ?></span>
                                <span>余额</span>
                            </a>
                            <?php if($php_servicemeal >= '1'): ?>
                            <a href="<?php echo url("user/UsersNotice/index","",true,false,null,null,null);?>" class="user-info-item">
                                <span><?php echo $allNotice_num; if($unread_num > '0'): ?><i class="badge"></i><?php endif; ?></span>
                                <span>消息</span>
                            </a>
                            <?php endif; ?>
                            <a href="<?php echo url("user/Users/footprint_index","",true,false,null,null,null);?>" class="user-info-item">
                                <span><?php echo $others['footprint_num']; ?></span>
                                <span>足迹</span>
                            </a>
                        </div>
                        <!-- 信息统计 end -->
                    </div>
                </div>
            </div>  
                
            <div class="nav-row mt15">
                <div class="goods-nav-main">
                    <div class="goods-nav">
                        <?php if(1 < $php_servicemeal && isset($others['signin_conf']) && isset($others['signin_conf']['score_signin_status']) && $others['signin_conf']['score_signin_status'] == 1): if(!$others['signin_info']): ?>
                            <span class="nav-item" id="user_signin_ahref" <?php if(!$others['signin_info']): ?> data-url="<?php echo url('api/Ajax/signin_save'); ?>" onclick="userSignin(this);" <?php endif; ?>>
                                <span class="nav-item-icon">
                                    <i class="iconfont icon-qiandao" style="color:#35dabb;"></i>
                                </span>
                                <span class="link" id="user_signin">
                                    签到
                                </span>
                            </span>
                            <?php else: ?>
                            <span class="nav-item">
                                <span class="nav-item-icon">
                                        <i class="iconfont icon-qiandao" style="color:#35dabb;"></i>
                                </span>
                                <span class="link">
                                    已签到
                                </span>
                            </span>
                            <?php endif; endif; if(isset($usersConfig['level_member_upgrade']) && $usersConfig['level_member_upgrade'] == 1): ?>
                        <a href="<?php echo url("user/Level/level_centre","",true,false,null,null,null);?>" class="nav-item">
                            <span class="nav-item-icon">
                                <i class="iconfont icon-huiyuanshengji" style="color:#ffc107;"></i>
                            </span>
                            <?php if($users['level'] > '1'): ?>
                                <span>升级会员</span>
                            <?php else: ?>
                                <span>开通会员</span>
                            <?php endif; ?>
                        </a>
                        <?php endif; ?>
                        <a href="<?php echo url("user/Users/collection_index","",true,false,null,null,null);?>" class="nav-item">
                            <span class="nav-item-icon">
                                <i class="iconfont icon-shoucang" style="color:#ffc107;"></i>
                            </span>
                            <span>收藏</span>
                        </a>
                        <a href="<?php echo url("user/Pay/pay_account_recharge","",true,false,null,null,null);?>" class="nav-item">
                            <span class="nav-item-icon">
                                <i class="iconfont icon-yue" style="color:#fe755b;"></i>
                            </span>
                            <span>账户充值</span>
                        </a>
                    </div>
                </div>
            </div>
            <!-- 商城导航 start -->
            <?php if(isset($usersConfig['shop_open']) && $usersConfig['shop_open'] == 1 && $php_servicemeal > 1): ?>
            <div class="nav-row mt15">
                <div class="goods-nav-main">
                    <div class="column-title">
                        <div class="column-title-l">我的订单</div>
                         <div class="column-title-r"><a href="<?php echo url("user/Shop/shop_centre","",true,false,null,null,null);?>">更多</a></div>  
                    </div>
                    <div class="goods-nav mt10">
                        <a href="<?php echo url("user/Shop/shop_centre","",true,false,null,null,null);?>" class="nav-item">
                            <span class="nav-item-icon2">
                                <i class="iconfont icon-dingdan"></i>
                            </span>
                            <span>全部订单</span>
                        </a>
                        <a href="<?php echo url('user/Shop/shop_centre', ['select_status'=>'dzf']); ?>" class="nav-item">
                            <span class="nav-item-icon2">
                                <i class="iconfont icon-daifukuan"></i>
                            </span>
                            <span>待付款</span>
                        </a>
                        <a href="<?php echo url('user/Shop/shop_centre', ['select_status'=>2]); ?>" class="nav-item">
                            <span class="nav-item-icon2">
                                <i class="iconfont icon-daishouhuo"></i>
                            </span>
                            <span>待收货</span>
                        </a>
                        <a href="<?php echo url('user/Shop/shop_centre', ['select_status'=>3]); ?>" class="nav-item">
                            <span class="nav-item-icon2">
                                <i class="iconfont icon-wancheng"></i>
                            </span>
                            <span>已完成</span>
                        </a>
                        <?php if($php_servicemeal > '1'): ?>
                        <a href="<?php echo url('user/Shop/after_service'); ?>" class="nav-item">
                            <span class="nav-item-icon2">
                                <i class="iconfont icon-shouhou"></i>
                            </span>
                            <span>售后</span>
                        </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <!-- 商城导航 end -->
            <!-- 导航列表 start -->
            <div class="ey-row">
                <div class="item-list">
                    <a href="<?php echo url("user/Users/info","",true,false,null,null,null);?>" class="item">
                        <div class="item-l">个人信息</div> 
                        <div class="item-m tr"></div> 
                        <div class="item-r"><i class="el-icon-arrow-right"></i></div>
                    </a> 
                    <?php if(isset($usersConfig['shop_open']) && $usersConfig['shop_open'] == 1): ?>
                    <a href="<?php echo url("user/Shop/shop_address_list","",true,false,null,null,null);?>" class="item">
                        <div class="item-l">收货地址</div> 
                        <div class="item-m tr"></div> 
                        <div class="item-r"><i class="el-icon-arrow-right"></i></div>
                    </a>
                    <?php endif; if(isset($usersConfig['users_open_release']) && $usersConfig['users_open_release'] == 1): ?>
                    <a href="<?php echo url('user/UsersRelease/release_centre', ['list'=>1]); ?>"  class="item">
                        <div class="item-l">投稿中心</div> 
                        <div class="item-m tr"></div> 
                        <div class="item-r"><i class="el-icon-arrow-right"></i></div>
                    </a>
                    <?php endif; if(isset($part_channel['download']['status']) && $part_channel['download']['status'] == 1): ?>
                    <a href="<?php echo url("user/Download/index","",true,false,null,null,null);?>" class="item">
                        <div class="item-l">我的下载</div>
                        <div class="item-m tr"></div> 
                        <div class="item-r"><i class="el-icon-arrow-right"></i></div>
                    </a>
                    <?php endif; if(isset($part_channel['article']['data']['is_article_pay']) && $part_channel['article']['data']['is_article_pay'] == 1): ?>
                    <a href="<?php echo url("user/Users/article_index","",true,false,null,null,null);?>" class="item">
                        <div class="item-l">我的文章</div>
                        <div class="item-m tr"></div>
                        <div class="item-r"><i class="el-icon-arrow-right"></i></div>
                    </a>
                    <?php endif; if(isset($part_channel['media']['status']) && $part_channel['media']['status'] == 1 && $php_servicemeal > 1): ?>
                    <a href="<?php echo url("user/Users/media_index","",true,false,null,null,null);?>" class="item">
                        <div class="item-l">我的视频</div>
                        <div class="item-m tr"></div> 
                        <div class="item-r"><i class="el-icon-arrow-right"></i></div>
                    </a>
                    <?php endif; if(isset($usersConfig['shop_open']) && $usersConfig['shop_open'] == 1 && $php_servicemeal >= 2): ?>
                    <a href="<?php echo url("user/ShopComment/index","",true,false,null,null,null);?>" class="item">
                        <div class="item-l">我的评价</div>
                        <div class="item-m tr"></div> 
                        <div class="item-r"><i class="el-icon-arrow-right"></i></div>
                    </a>
                    <?php endif; if(isset($part_channel['ask']['status']) && $part_channel['ask']['status'] == 1 && $php_servicemeal >= 2): ?>
                    <a href="<?php echo url("user/Ask/ask_index","",true,false,null,null,null);?>" class="item">
                        <div class="item-l">我的问答</div>
                        <div class="item-m tr"></div>
                        <div class="item-r"><i class="el-icon-arrow-right"></i></div>
                    </a>
                    <?php endif; ?>
                </div>
            </div>
            
            <!-- 导航列表 end -->
            <div class="user-logout mb10">
                <a class="el-button el-button--primary el-button--medium" href="<?php echo url("user/Users/logout","",true,false,null,null,null);?>">退出登录</a>
            </div>
                       
            <!-- 底部导航 start -->
            <div class="h50"></div>
            <div class="footer-nav">
				<?php if(is_array($bottom_menu_list) || $bottom_menu_list instanceof \think\Collection || $bottom_menu_list instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $bottom_menu_list;if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$vo): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
				<a href="<?php echo url($vo['mca']); ?>" class="nav-item">
					<div class="nav-item-t"><i class="iconfont icon-<?php echo $vo['icon']; ?>"></i></div>
					<div class="nav-item-b"><?php echo $vo['title']; ?></div>
				</a>
				<?php echo isset($vo["ey_1563185380"])?$vo["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($vo["ey_1563185376"]))?$vo["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $vo = []; ?>
            </div>
            <!-- 底部导航 end -->
        </div>

        <script type="text/javascript">
            // 签到
            function userSignin(obj) {
                layer_loading('正在处理');
                $.ajax({
                    type: "POST",
                    url: $(obj).attr('data-url'),
                    data: {_ajax:1},
                    dataType: 'json',
                    success: function (res) {
                        layer.closeAll();
                        if(res.code == 1){
                            layer.msg(res.msg, {icon: 6, time:1500});
                            $("#user_signin").html("已签到");
                            $("#user_signin_ahref").removeAttr("onclick");
                            $("#user_signin_ahref").removeAttr("data-url");
                            $("#users_scores").html(res.data.scores);
                        }else{
                            showErrorAlert(res.msg);
                        }
                    },
                    error:function(e){
                        layer.closeAll();
                        showErrorAlert(e.responseText);
                    }
                });
            }
        </script>

    </body>
</html>
