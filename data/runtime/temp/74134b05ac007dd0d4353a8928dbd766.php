<?php if (!defined('THINK_PATH')) exit(); /*a:6:{s:40:"./template/mobile/users/users_centre.htm";i:1648540172;s:59:"/data/www/jiaoyu/template/mobile/users/skin/css/diy_css.htm";i:1648540172;s:55:"/data/www/jiaoyu/template/mobile/users/users_header.htm";i:1648540172;s:61:"/data/www/jiaoyu/template/mobile/users/users_centre_field.htm";i:1648540172;s:65:"./public/static/template/users_v2/users_centre_field_mobile_m.htm";i:1616029662;s:65:"./public/static/template/users_v2/users_centre_field_extend_m.htm";i:1657524382;}*/ ?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8" />
    <title>我的信息-<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_name"); echo $__VALUE__; ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <link href="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_cmspath"); echo $__VALUE__; ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/basic.css","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/eyoucms.css","","",""); echo $__VALUE__; ?>
    <!-- 新样式 2020-12-8 -->
    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/element/index.css","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/e-user.css","","",""); echo $__VALUE__; ?>
    
<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style>
  /*
  <?php echo $theme_color; ?>
  <?php echo hex2rgba($theme_color,0.8); ?> */
    a:hover {
        color: <?php echo $theme_color; ?>;
    }
    .ey-container .ey-header-top {
        background: linear-gradient(90deg, <?php echo $theme_color; ?> 10%, <?php echo hex2rgba($theme_color,0.8); ?> 60%);
    }
    .ey-container .ey-con .oper-row .el-button.active {
        background-color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .el-button--primary {
        background-color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .el-button--primary.is-plain {
        color: <?php echo $theme_color; ?>;
        border-color: <?php echo hex2rgba($theme_color,0.2); ?>;
        background-color:<?php echo hex2rgba($theme_color,0.1); ?>;
    }
    .el-button--primary.is-plain:focus, .el-button--primary.is-plain:hover {
        background: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .el-button--primary:hover, .el-button--primary:focus {
        background: <?php echo hex2rgba($theme_color,0.9); ?>;
        border-color: <?php echo hex2rgba($theme_color,0.9); ?>;
        color: #fff;
    }
    .el-input.is-active .el-input__inner, .el-input__inner:focus {
        border-color:<?php echo $theme_color; ?>;
    }
    .ey-container .radio-label .radio:checked+.check-mark {
        border: 1px solid <?php echo $theme_color; ?>;
        background-color: <?php echo $theme_color; ?>;
    }
    .ey-container .radio-label .radio:checked+.check-mark:after {
        background:<?php echo $theme_color; ?>;
    }
    .ey-container .item-from-flex .item-con .link a {
        color:<?php echo $theme_color; ?>;
    }
    .iphone_z {
        border: 1px solid <?php echo $theme_color; ?>;
        color: <?php echo $theme_color; ?>;
    }
    .email_z {
        border: 1px solid <?php echo $theme_color; ?>;
        color: <?php echo $theme_color; ?>;
    }
    .ey-container .item-from-flex .item-con .checkbox-label .checkbox:checked+.check-mark {
        border: 1px solid <?php echo $theme_color; ?>;
        background-color: <?php echo $theme_color; ?>;
    }
    .ey-container .item-from-flex .item-con .radio-label .radio:checked+.check-mark {
        border: 1px solid <?php echo $theme_color; ?>;
        background-color: <?php echo $theme_color; ?>;
    }
    .ey-container .address-add {
        background-color:<?php echo $theme_color; ?>;
    }
    .ey-container .address-con .address-item.cur {
        border: 1px solid <?php echo hex2rgba($theme_color,0.6); ?>;
    }
    .ey-container .address-con .address-item.cur:before {
        color:<?php echo $theme_color; ?>;
        border-top: 50px solid <?php echo $theme_color; ?>;
    }
    .ey-container div.dataTables_paginate .paginate_button.active>a, .ey-containerdiv.dataTables_paginate .paginate_button.active>a:focus, .ey-containerdiv.dataTables_paginate .paginate_button.active>a:hover {
        background: <?php echo $theme_color; ?> !important;
        border-color: <?php echo $theme_color; ?> !important;
    }
    .ey-container .pagination>li>a, .ey-container .pagination>li>span {
        color: <?php echo $theme_color; ?>;
    }
    .btn-primary{
        background-color: <?php echo $theme_color; ?> !important;
    }
    .list_z .re_ed {
        color:<?php echo $theme_color; ?>;
    }
    .btn_zb{
        background-color: <?php echo $theme_color; ?> !important;
    }
    .el-button--danger {
        background-color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>
    }
    .my-top {
        background: <?php echo hex2rgba($theme_color,0.1); ?>;
    }
    .m-user-title {
        color: <?php echo $theme_color; ?>;
    }
    .recharge .pc-vip-list.active {
        border: <?php echo $theme_color; ?> 1px solid;
        background-color:<?php echo hex2rgba($theme_color,0.1); ?>;
    }
    .recharge .pc-vip-list .icon-recomd {
        background: <?php echo $theme_color; ?>;
    }
    .recharge .pc-vip-list .pc-vip-name {
        color: <?php echo $theme_color; ?>;
    }
    .recharge .pc-vip-list .money {
        color: <?php echo $theme_color; ?>;
    }
    .recharge .pc-vip-list .money em {
        color: <?php echo $theme_color; ?>;
    }
    .button2 {
        background: <?php echo $theme_color; ?>;
    }
    .el-button:focus, .el-button:hover {
        color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
        background-color: <?php echo hex2rgba($theme_color,0.1); ?>;
    }
    .ey-container .checkbox-label .checkbox:checked+.check-mark {
        background-color: <?php echo $theme_color; ?>;
        border: 1px solid <?php echo $theme_color; ?>;
    }
    .ey-container .checkbox-label .checkbox:checked+.check-mark:after {
        background:<?php echo $theme_color; ?>;
    }
    .ey-container .pay-type .payTag a {
        color: <?php echo $theme_color; ?>;
        border-bottom: 1px solid <?php echo $theme_color; ?>;
    }
    .ey-container .column-title .column-name {
        color:<?php echo $theme_color; ?>;
    }
    .ey-container .pay-type .pay-con .pay-type-item.active:before {
        color: <?php echo $theme_color; ?>;
        border-bottom: 30px solid <?php echo $theme_color; ?>;
    }
    .ey-container .pay-type .pay-con .pay-type-item.active {
        border-color: <?php echo hex2rgba($theme_color,0.4); ?>;
    }
    .ey-popups .switchCheck :checked + i {
        background: <?php echo $theme_color; ?>;   
    }
    
</style>

<script type="text/javascript">
    var __root_dir__ = "";
    var __version__ = "<?php echo $version; ?>";
    var __lang__ = "<?php echo $home_lang; ?>";
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
</script>

    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/static/common/js/jquery.min.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/js/bootstrap.min.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/js/global.js","","",""); echo $__VALUE__; ?>
</head>

<body>
        <!-- 头像上传 -->
    <div id="update_mobile_file" style="display: none;">
        <form id="form1" style="text-align: center;" >
            <input type="button" value="点击上传" onclick="up_f.click();" class="btn btn-primary"/><br>
            <p><input type="file" id="up_f" name="up_f" onchange="MobileHeadPic();" style="display:none"/></p>
        </form>
    </div>

    <script type="text/javascript">
        function MobileHeadPic(){
            $.getScript('/public/plugins/layer_mobile/layer.js?v=<?php echo $version; ?>', function(){
                // 提示信息，2秒自动关闭
                function MsgOpen(msgs){
                    layer.open({
                        content: msgs,
                        skin: 'msg',
                        time: 2,
                    });
                }

                // 提示信息，估计在底部提示，点击空白处关闭
                function FooterOpen(msgs){
                    layer.open({
                        content: msgs,
                        skin: 'footer',
                    });
                }

                // 提示动画
                function LoaDing(){
                    var loading = layer.open({
                        type:2,
                        content: '正在处理',
                    });
                    return loading;
                }

                UpdateMobileHeadPic();

                // 移动端更换头像
                function UpdateMobileHeadPic()
                {
                    // 正在处理提示动画
                    var loading = LoaDing();
                    // 获取表单对象
                    var data = new FormData($('#form2')[0]);
                    // 上传类型
                    var UpFileType = $('#UpFileType').val();
                    $.ajax({
                        url: "<?php echo url("user/Uploadify/imageUp","savepath=allimg&pictitle=head_pic&dir=images",true,false,null,null,null);?>", 
                        type: 'post',  
                        data: data,  
                        dataType: 'json',
                        cache: false,  
                        processData: false,  
                        contentType: false,
                        success:function(res){
                            if (res.state == 'SUCCESS') {
                                layer.closeAll();
                                MsgOpen('上传成功！');
                                if (1 == UpFileType) {
                                    parent.$("#litpic_inpiut").val(res.url);
                                    parent.$("#litpic_img").attr('src', res.url);
                                }else{
                                    MobileHeadPic(res.url);
                                }
                            }else{
                                layer.close(loading);
                                MsgOpen(res.state);
                            }
                        },
                        error : function(e) {
                            layer.close(loading);
                            FooterOpen(e.responseText);
                        }
                    });
                };

                // 上传头像成功后加载到页面
                function MobileHeadPic(fileurl_tmp)
                {
                    $("#head_pic").val(fileurl_tmp);
                    $("#head_pic_a").attr('src', fileurl_tmp);
                    // 正在处理提示动画
                    var loading = LoaDing();
                    $.ajax({
                        url: "<?php echo url("user/Users/edit_users_head_pic","",true,false,null,null,null);?>",
                        data: {filename:fileurl_tmp},
                        type:'post',
                        dataType:'json',
                        success:function(res){
                            if (1 == res.code) {
                                layer.closeAll();
                                MsgOpen(res.msg);
                            } else {
                                layer.close(loading);
                                MsgOpen(res.msg);
                            }
                        }
                    });
                }
            })
        }
    </script>
    <!-- 头像上传结束 -->

<!-- 头像默认更换路径 -->
<script type="text/javascript">
    var GetUploadify_url = "<?php echo url("user/Uploadify/upload","",true,false,null,null,null);?>";
</script>


    <!-- 头部信息 -->
    <div class="ey-header-status">
        <div class="header-status-l">
            <a href="javascript:history.go(-1)"><i class="el-icon-arrow-left"></i></a>
        </div> 
        <div class="header-status-m">个人设置</div> 
        <div class="header-status-r">
            <a href="<?php echo url("user/Users/index","",true,false,null,null,null);?>"><i class="el-icon-user"></i></a>
        </div>
    </div>
    <!-- 头部信息结束 -->
    <div class="h50"></div>
    <div class="ey-container">
        <div class="item-list">
            <div class="item">
                <div class="item-l">
                    <div class="face">
                        <div  class="el-upload el-upload--text" onclick="$('#ey_head_pic').trigger('click');">
                            <img id="ey_head_pic_a" src="<?php echo get_head_pic($users['head_pic']); ?>"/>
                            <span title="编辑头像" class="el-icon-edit-outline"></span>
                        </div>
                        <input type="file" name="ey_head_pic" id="ey_head_pic" data-max_file_size="2" onchange="upload_head_pic(this)" style="display: none;">
                    </div>
                </div> 
                
                <div class="item-m fs16"><?php echo $nickname; ?></div> 
                <div class="item-r"><span class="member fs16"><?php echo $users['level_name']; ?></span></div>
            </div>
        </div>
        <div class="ey-con ey-row mt10">
            <div class="item-from-flex">
                
                <div class="item-flex wb25">
                    <div class="item-tit fs16">
                        用户名
                    </div>
                </div>
                
                <div class="item-flex-r flex wb75 fs16 tar_z">
                    <?php echo $users['username']; ?>
                </div>
                <div class="item-flex-r"></div>
            </div>
        </div>
        <div class="ey-con ey-row mt10">
            <div class="item-from-flex">
                    
                    <div class="item-flex wb25">
                        <div class="item-tit fs16">
                            修改密码
                        </div>
                    </div>
                    <div class="item-flex-r flex wb75">
                        <div class="item-con wb100">
                            <input type="text" name="password_edit" id="password_edit" autocomplete="off" placeholder="留空时默认不修改密码" class="input-text fs16 tar_z">
                        </div>
                    </div>
                <div class="item-flex-r">
                    <i class="el-icon-arrow-right"></i>
                </div>
            </div>
        </div>
        <form name='theForm' id="theForm">
            <div class="ey-con ey-row mt10">
                <div class="item-from-flex">
                    <div class="item-flex wb25">
                        <div class="item-tit fs16">
                            我的昵称
                        </div>
                    </div>
                    <div class="item-flex-r flex wb75">
                        <div class="item-con wb100">
                            <input type="text" name="nickname" value="<?php echo $users['nickname']; ?>" class="input-text fs16 tar_z">
                        </div>
                    </div>
                    <div class="item-flex-r">
                        <i class="el-icon-arrow-right"></i>
                    </div>
                </div>
            </div>
            <?php if(empty($users['password']) || (($users['password'] instanceof \think\Collection || $users['password'] instanceof \think\Paginator ) && $users['password']->isEmpty())): ?>
            <div class="ey-con ey-row mt10">
                <div class="item-from-flex">
                    <div class="item-flex wb25">
                        <div class="item-tit fs16">
                            设置密码
                        </div>
                    </div>
                    <div class="item-flex-r flex wb75">
                        <div class="item-con wb100">
                            <input type="text" name="password" id="password" class="input-text tar_z" placeholder="微信注册用户，请设置密码">
                        </div>
                    </div>
                    <div class="item-flex-r">
                        <i class="el-icon-arrow-right"></i>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <!-- 更多资料中的会员属性 -->
            <?php if(is_array($users_para) || $users_para instanceof \think\Collection || $users_para instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $users_para;if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$vo): $i= intval($key) + 1;$mod = ($i % 2 ); switch($vo['dtype']): case "hidden": ?>
                <!-- 隐藏域 start -->
                <div class="item-from-row" style="display: none;">
                    <dt class="tit">
                        &nbsp;&nbsp;<label><?php echo $vo['title']; ?></label>
                    </dt>
                    <dd class="opt">
                        <input type="hidden" class="input-txt" id="<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>]" value="<?php echo (isset($vo['dfvalue']) && ($vo['dfvalue'] !== '')?$vo['dfvalue']:''); ?>">
                        <span class="err"></span>
                        <p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>
                    </dd>
                </div>
                <!-- 隐藏域 start -->
            <?php break; case "mobile": ?>
                <!-- 手机文本框 start -->
                <div class="ey-con ey-row mt10">
    <div class="item-from-flex">
        <div class="item-flex wb25">
            <div class="item-tit fs16">
                <!-- <?php if(1 == $vo['is_required']): ?>
                    <span class="red">*</span>
                <?php else: ?>
                    <span class="red"></span>
                <?php endif; ?> -->
                <?php echo $vo['title']; ?>
            </div>
        </div>
        <div class="item-flex-r flex wb75">
            <div class="item-con wb100 tar_z">
                <span class="tit fs16"><?php echo (isset($vo['dfvalue']) && ($vo['dfvalue'] !== '')?$vo['dfvalue']:''); ?></span>
                <?php if($users['is_mobile'] == '1'): ?>
                    <span class="link"><a href="JavaScript:void(0);" onclick="BindUpdateMobile('更改手机');" class="iphone_z">更改手机</a></span>
                <?php else: ?>
                    <span class="link"><a href="JavaScript:void(0);" onclick="BindUpdateMobile('绑定手机');" class="iphone_z">绑定手机</a></span>
                <?php endif; ?>
                <div id="users_bind_mobile_html" style="display: none;">
                    <div class="el-row">
                        <eyoucms_form_2020 name='theForm_mobile_form' id="theForm_mobile_form" method="post">
                            <div class="ey-popup">
                                 <div class="el-form-item">
                                     <div class="el-input">
                                         <input type="text" id="bind_mobile_old" name="mobile" <?php if($users['is_mobile'] == '0'): ?> value="<?php echo $users['mobile']; ?>" <?php endif; ?> required class="el-input__inner" placeholder="新的手机号码">
                                     </div>
                                 </div>
                                 <div class="el-form-item">
                                     <div class="el-input el-input-group el-input-group--append el-input-group--prepend">
                                          <input type="text" class="el-input__inner" id="bind_mobile_code" name="mobile_code" placeholder="手机验证码">
                                          <div class="el-input-group__append">
                                             <input type="button" id="bind_mobile_button" onclick="GetMobileCodeMobile();" class="el-button el-button--default" value="获取验证码" />
                                          </div>  
                                     </div>
                                 </div>
                                 <div class="el-form-item">
                                      <div class="el-input">
                                           <button type="button" class="el-button el-button--primary" onclick="SubmitUpdateMobile();" style="width:86%;">确定</button>
                                      </div>
                                 </div>
                                 
                             </div>
                            
                           <!-- <div class="modal-body">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" id="bind_mobile_old" name="mobile" <?php if($users['is_mobile'] == '0'): ?> value="<?php echo $users['mobile']; ?>" <?php endif; ?> required class="form-control fs16" placeholder="新的手机号码">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group" style="position: relative;">
                                        <input type="text" class="form-control" id="bind_mobile_code" name="mobile_code" placeholder="手机验证码">
                                        <input type="button" id="bind_mobile_button" onclick="GetMobileCodeMobile();" class="btn btn-primary" value="获取验证码" />
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary btn_zb" onclick="SubmitUpdateMobile();">确定</button>
                            </div> -->
                        </eyoucms_form_2020>
                    </div>
                </div>
                <script type="text/javascript">
                    // 绑定、更换手机号码
                    function BindUpdateMobile(title) {
                        var content = $('#users_bind_mobile_html').html();
                        content = content.replace(/eyoucms_form_2020/g, 'form');
                        content = content.replace(/theForm_mobile_form/g, 'bind_mobile_form_2020');
                        content = content.replace(/bind_mobile_old/, 'bind_mobile_new_2020');
                        content = content.replace(/bind_mobile_code/, 'bind_mobile_code_2020');
                        content = content.replace(/bind_mobile_button/, 'bind_mobile_button_2020');
                        layer.open({
                            type: 1,
                            title: title,
                            style:'position:fixed; bottom:0; left:0; width: 100%; padding:10px 0; border:none;max-width: 100%;',
                            anim:'up',
                            content: content,
                        });
                    }

                    // 获取手机验证码
                    function GetMobileCodeMobile() {
                        // 正在处理提示动画
                        var loading = loa_ding();
                        // 标题
                        var title = $('h3').html();
                        // 手机号码
                        var mobile = $("#bind_mobile_new_2020").val();
                        // 手机号是否为空
                        if (!mobile) {
                            layer.close(loading);
                            $("#bind_mobile_new_2020").focus();
                            msg_open('请输入新的手机号码！');
                            return false;
                        }
                        
                        // 手机格式不正确
                        var mobile_format = /^1[0-9]{10}$/i;
                        if (!mobile_format.test(mobile)) {
                            layer.close(loading);
                            $("#bind_mobile_new_2020").focus();
                            msg_open('请输入正确的新的手机号码！');
                            return false;
                        }

                        // 发送验证码
                        $("#bind_mobile_button_2020").val('发送中…');
                        $.ajax({
                            url: '<?php echo $RootDir; ?>/index.php?m=api&c=Ajax&a=SendMobileCode&_ajax=1',
                            data: {mobile: mobile, is_mobile: true, source: 1},
                            type:'post',
                            dataType:'json',
                            success:function(res) {
                                layer.close(loading);
                                if (res.code == 1) {
                                    MobileCountDown();
                                    msg_open(res.msg);
                                } else {
                                    $("#bind_mobile_button_2020").val('获取验证码').removeAttr("disabled");
                                    msg_open(res.msg);
                                }
                            },
                            error : function() {
                                $("#bind_mobile_button_2020").val('获取验证码').removeAttr("disabled");
                                layer.close('发送失败，请尝试重新发送！');
                                msg_open(res.msg);
                            }
                        });
                    }

                    // 倒计时
                    function MobileCountDown() {
                        var setTime;
                        var time = 120;
                        setTime = setInterval(function(){
                            if(0 >= time){
                                clearInterval(setTime);
                                return;
                            }
                            time--;
                            $("#bind_mobile_button_2020").val(time+'秒');
                            $("#bind_mobile_button_2020").attr('disabled', 'disabled');

                            if(time == 0){
                                $("#bind_mobile_button_2020").val('获取验证码');
                                $("#bind_mobile_button_2020").removeAttr("disabled");
                            }
                        }, 1000);
                    }

                    // 提交手机及验证码进行绑定
                    function SubmitUpdateMobile() {   
                        var loading = loa_ding();// 正在处理提示动画

                        var mobile = $("#bind_mobile_new_2020").val();
                        var reg = /^1[0-9]{10}$/i;
                        // 邮箱格式不正确
                        if(!reg.test(mobile)) {
                            layer.close(loading);
                            $("#bind_mobile_new_2020").focus();
                            msg_open('请正确输入手机号码');
                            return false;
                        }

                        var mobile_code = $("#bind_mobile_code_2020").val();
                        if(!mobile_code) {
                            layer.close(loading);
                            $("#bind_mobile_code_2020").focus();
                            msg_open('请输入手机验证码');
                            return false;
                        }

                        $.ajax({
                            url: '<?php echo $RootDir; ?>/index.php?m=user&c=Users&a=bind_mobile&_ajax=1',
                            data: $('#bind_mobile_form_2020').serialize(),
                            type:'post',
                            dataType:'json',
                            success:function(res){
                                if (res.code == 1) {
                                    layer.closeAll();
                                    msg_open(res.msg);
                                    location.reload();
                                } else {
                                    layer.close(loading);
                                    msg_open(res.msg);
                                }
                            },
                            error : function() {
                                layer.close(loading);
                                footer_open('未知错误，请刷新重试');
                            }
                        });
                    };
                </script>
                <p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>
            </div>
        </div>
    </div>
</div>    
                <!-- 手机文本框 end -->
            <?php break; case "email": ?>
                <!-- 邮箱文本框 start -->
                <div class="ey-con ey-row mt10">
                	<div class="item-from-flex">
                		<div class="item-flex wb25">
                			<div class="item-tit fs16">
                				<?php echo $vo['title']; ?>
                			</div>
                        </div>
                        <div class="item-flex-r flex wb75">
                			<div class="item-con wb100 tar_z">
                                <span class="tit fs16"><?php echo (isset($vo['dfvalue']) && ($vo['dfvalue'] !== '')?$vo['dfvalue']:''); ?></span>
                                <!-- 手机端 -->
                                <?php if($is_mobile == '1'): if($users['is_email'] == '1'): ?>
                                        <span class="link"><a href="JavaScript:void(0);" onclick="BindEmailMobile('更改邮箱');" class="email_z">更改邮箱</a></span> 
                                    <?php else: ?>
                                        <span class="link"><a href="JavaScript:void(0);" onclick="BindEmailMobile('绑定邮箱');" class="email_z">绑定邮箱</a></span>
                                    <?php endif; else: ?>
                                    <!-- PC端 -->
                                    <?php if($users['is_email'] == '1'): ?>
                                        <span class="err"><a href="JavaScript:void(0);" onclick="get_<?php echo $vo['fieldArr']; ?><?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>_email_code('更改邮箱');" class="email_z">更改邮箱</a></span>
                                    <?php else: ?>
                                        <span class="err"><a href="JavaScript:void(0);" onclick="get_<?php echo $vo['fieldArr']; ?><?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>_email_code('绑定邮箱');" class="email_z">绑定邮箱</a></span>
                                    <?php endif; endif; ?>
                                <p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>    
                			</div>
                		</div>
                	</div>
                </div>
                <?php if($is_mobile == '2'): ?>
                    <script type="text/javascript">
                        function get_<?php echo $vo['fieldArr']; ?><?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>_email_code(title)
                        {
                            var url = "<?php echo url("user/Users/bind_email","",true,false,null,null,null);?>";
                            if (url.indexOf('?') > -1) {
                                url += '&';
                            } else {
                                url += '?';
                            }
                            url += 'title='+title;
                            //iframe窗
                            layer.open({
                                type: 2,
                                title: title,
                                shadeClose: false,
                                maxmin: false, //开启最大化最小化按钮
                                area: ['350px', '300px'],
                                content: url
                            });
                        }
                    </script>
                <?php endif; ?>
                <!-- 邮箱文本框 end -->
            <?php break; case "text": ?>
                <!-- 单行文本框 start -->
                <div class="ey-con ey-row mt10">
                	<div class="item-from-flex">
                		<div class="item-flex-l">
                			<div class="item-tit fs16">
                                <?php if(1 == $vo['is_required']): ?>
                                	<span class="red">*</span>
                                <?php else: ?>
                                    <span class="red"> </span>
                                <?php endif; ?>
                				<?php echo $vo['title']; ?>
                			</div>
                			<div class="item-con">
                                <input type="text" class="input-text fs16" id="<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>]" value="<?php echo (isset($vo['dfvalue']) && ($vo['dfvalue'] !== '')?$vo['dfvalue']:''); ?>"><?php echo (isset($vo['dfvalue_unit']) && ($vo['dfvalue_unit'] !== '')?$vo['dfvalue_unit']:''); ?>
                                <span class="err"></span>
                                <p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>
                			</div>
                		</div>
                		<div class="item-flex-r">
                			<i class="el-icon-arrow-right"></i>
                		</div>
                	</div>
                </div>
                <!-- 单行文本框 end -->
            <?php break; case "multitext": ?>
                <!-- 多行文本框 start -->
                <div class="ey-con ey-row mt10">
                	<div class="item-from-flex">
                		<div class="item-flex-l">
                			<div class="item-tit fs16">
                                <?php if(1 == $vo['is_required']): ?>
                                	<span class="red">*</span>
                                <?php else: ?>
                                    <span class="red"></span>
                                <?php endif; ?>
                				<?php echo $vo['title']; ?>
                			</div>
                			<div class="item-con">
                                 <textarea id="<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>]" class="fs16" style="height:60px;width:100%;" ><?php echo (isset($vo['dfvalue']) && ($vo['dfvalue'] !== '')?$vo['dfvalue']:''); ?></textarea>
                                <span class="err"></span>
                                <p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>
                			</div>
                		</div>
                	</div>
                </div>
                
                <!-- 多行文本框 end -->
            <?php break; case "checkbox": ?>
                <!-- 复选框 start -->
                <div class="ey-con ey-row mt10">
                	<div class="item-from-flex">
                		<div class="item-flex-l">
                			<div class="item-tit fs16">
                				<?php echo $vo['title']; ?>
                			</div>
                			<div class="item-con">
                				<?php if(is_array($vo['dfvalue']) || $vo['dfvalue'] instanceof \think\Collection || $vo['dfvalue'] instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $vo['dfvalue'];if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$v2): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
                				   <label class="checkbox-label">
                                        <input type="checkbox" class="checkbox" name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>][]" value="<?php echo $v2; ?>" <?php if(isset($vo['trueValue']) AND in_array($v2, $vo['trueValue'])): ?>checked="checked"<?php endif; ?>>
                						<span class="check-mark"></span>
                                        <span class="text"><?php echo $v2; ?></span>
                					</label>
                				<?php echo isset($v2["ey_1563185380"])?$v2["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($v2["ey_1563185376"]))?$v2["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $v2 = []; if(1 == $vo['is_required']): ?>
                                    <p>（必选）</p>
                                <?php endif; ?>
                				<span class="err"></span>
                				<p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>
                			</div>
                		
                		</div>
                	</div>
                </div>
                <!-- 复选框 end -->
            <?php break; case "radio": ?>
                <!-- 单选项 start -->
                <div class="ey-con ey-row mt10">
                    <div class="item-from-flex">
                        <div class="item-flex-l">
                            <div class="item-tit fs16">
                                <?php echo $vo['title']; ?>
                            </div>
                            <div class="item-con">
                                <?php if(is_array($vo['dfvalue']) || $vo['dfvalue'] instanceof \think\Collection || $vo['dfvalue'] instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $vo['dfvalue'];if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$v2): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
                                   <label class="radio-label">
                                        <input type="radio" class="radio" name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>]" value="<?php echo $v2; ?>" <?php if(isset($vo['trueValue']) AND in_array($v2, $vo['trueValue'])): ?>checked="checked"<?php endif; ?>>
                                        <span class="check-mark"></span>
                                        <span class="text"><?php echo $v2; ?></span>
                                    </label>
                                <?php echo isset($v2["ey_1563185380"])?$v2["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($v2["ey_1563185376"]))?$v2["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $v2 = []; if(1 == $vo['is_required']): ?>
                                    <p>（必选）</p>
                                <?php endif; ?>
                                <span class="err"></span>
                                <p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>
                            </div>
                        
                        </div>
                    </div>
                </div>
                <!-- 单选项 end -->
            <?php break; case "select": ?>
                <!-- 下拉框 start -->
                <div class="ey-con ey-row mt10">
                	<div class="item-from-flex">
                		<div class="item-flex wb25">
                			<div class="item-tit fs16">
                                <?php if(1 == $vo['is_required']): ?>
                				<span class="red">*</span> 
                                <?php endif; ?>
                                <?php echo $vo['title']; ?>
                			</div>
                		</div>
                		<div class="item-flex-r wb75">
                			<div class="select">
                				<select name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>]" id="<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>">
                				    <option value="">请选择</option>
                				    <?php if(is_array($vo['dfvalue']) || $vo['dfvalue'] instanceof \think\Collection || $vo['dfvalue'] instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $vo['dfvalue'];if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$v2): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
                				        <option value="<?php echo $v2; ?>" <?php if(isset($vo['trueValue']) AND in_array($v2, $vo['trueValue'])): ?>selected<?php endif; ?>><?php echo $v2; ?></option>
                				    <?php echo isset($v2["ey_1563185380"])?$v2["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($v2["ey_1563185376"]))?$v2["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $v2 = []; ?>
                				</select>
                			</div>
                            <span class="err"></span>
                            <p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>
                		</div>
                	</div>
                </div>
                
                <!-- 下拉框 end -->
            <?php break; ?>
            <!-- 扩展 start -->
            <!-- 扩展 -->
<?php case "img": ?>
<!-- 单张图 start -->
<div class="ey-con ey-row mt10">
        <div class="item-from-flex">
            <div class="item-flex wb25">
            	<div class="item-tit fs16">
            		<?php echo $vo['title']; ?>
            	</div>
            </div>
            <div class="item-flex-r wb75">
                <div class="item-flex-img">
                    <img id="single_img_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" class="img-fluid img1_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>"  onclick="$('#upload_single_pic_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>').trigger('click');" src="<?php echo (isset($vo['info']) && ($vo['info'] !== '')?$vo['info']:'/public/static/common/images/not_upload_pic.png'); ?>"/>
                    <input type="file" name="upload_single_pic" id="upload_single_pic_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" onchange="upload_single_pic_1609837252(this,'<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>')" style="display: none;">
                    <input type="hidden" class="type-file-text" id="single_pic_input_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>]" value="<?php echo (isset($vo['info']) && ($vo['info'] !== '')?$vo['info']:''); ?>">
                </div>
            </div>
            
            <div class="item-flex-r">
                <i class="el-icon-arrow-right"></i>
            </div>
        </div>
    </div>
<!-- 单张图 end -->
<?php break; case "file_tmp":  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/layui/css/layui.css","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/layui/layui.js","","",""); echo $__VALUE__; ?>
    <!-- 单个文件 start -->
    <div class="ey-con ey-row mt10">
    	<div class="item-from-flex">
    		<div class="item-flex wb25">
    			<div class="item-tit fs16">
    				<?php echo $vo['title']; ?>
    			</div>
    		</div>
    		<div class="item-flex-r w6rem tar_z">
    			<a id="download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" style="margin-right: 30px;text-decoration: underline;<?php if(!(empty($vo['dfvalue']) || (($vo['dfvalue'] instanceof \think\Collection || $vo['dfvalue'] instanceof \think\Paginator ) && $vo['dfvalue']->isEmpty()))): ?>display: '';<?php else: ?>display: none;<?php endif; ?>"
    			   <?php if(!(empty($vo['dfvalue']) || (($vo['dfvalue'] instanceof \think\Collection || $vo['dfvalue'] instanceof \think\Paginator ) && $vo['dfvalue']->isEmpty()))): ?> href="<?php echo (isset($vo['dfvalue']) && ($vo['dfvalue'] !== '')?$vo['dfvalue']:''); ?>" download="<?php echo get_filename($vo['dfvalue']); ?>" <?php endif; ?>>
    			<img src="/public/static/common/images/file.png" alt="" style="width: 16px;height:  16px;">下载附件</a>
    			<input type="text" id="<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>]" value="<?php echo (isset($vo['dfvalue']) && ($vo['dfvalue'] !== '')?$vo['dfvalue']:''); ?>" style="display: none;">
    			<button type="button" class="el-button el-button--primary is-plain el-button--small" id="upload_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" ><i class="el-icon-paperclip"></i>上传文件</button>
    		</div>
    		<div class="item-flex-r">
    			<i class="el-icon-arrow-right"></i>
    		</div>
    	</div>
    </div>
    
    <script type="text/javascript">
        $(function(){
            layui.use('upload', function(){
                var upload = layui.upload,
                    layer = layui.layer;

                //执行实例
                var uploadInst = upload.render({
                    elem: "#upload_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" //绑定元素
                    ,url: "<?php echo url('user/Uploadify/DownloadUploadFileAjax'); ?>"
                    ,accept: 'file' //普通文件
                    ,exts: '<?php echo $vo['ext']; ?>'
                    ,size: <?php echo $vo['filesize']; ?> //限制文件大小，单位 KB
                    ,done: function(res){
                        //上传完毕回调
                        if (res.state=="SUCCESS"){
                            layer.msg('上传成功!')
                            $("#<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").val(res.url);
                            $("#download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").css('display','');
                            $("#download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").attr('href',res.url);
                            var arr = res.url.split("/");
                            var download = arr[arr.length-1];
                            $("#download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").attr('download',download);
                            $("#download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").css('color','#000');
                            $("#download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").html('<img src="/public/static/common/images/file.png" alt="" style="width: 16px;height:  16px;">下载附件');
                        }else {
                            $("#download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").css('display','');
                            $("#download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").css('color','red');
                            $("#download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").text(res.state);
                        }
                    }
                    ,error: function(){
                        //请求异常回调
                    }
                });
            });
        })
    </script>
    <!-- 单个文件 end -->
<?php break; case "datetime": ?>
    <!-- 日期和时间 start -->
    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/layui/css/layui.css","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/layui/layui.js","","",""); echo $__VALUE__; ?>
    <div class="ey-con ey-row mt10">
        <div class="item-from-flex">
            <div class="item-flex-l">
                <div class="item-tit fs16">
                    <?php if(1 == $vo['is_required']): ?>
                        <span class="red">*</span>
                    <?php else: ?>
                        <span class="red"> </span>
                    <?php endif; ?>
                    <?php echo $vo['title']; ?>
                </div>
                <div class="item-con">
                    <input type="text" class="input-text fs16" id="<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>]" value="<?php echo (isset($vo['dfvalue']) && ($vo['dfvalue'] !== '')?$vo['dfvalue']:''); ?>"><?php echo (isset($vo['dfvalue_unit']) && ($vo['dfvalue_unit'] !== '')?$vo['dfvalue_unit']:''); ?>
                    <span class="err"></span>
                    <p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>
                </div>
            </div>
            <div class="item-flex-r">
                <i class="el-icon-arrow-right"></i>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        layui.use('laydate', function() {
            var laydate = layui.laydate;

            laydate.render({
                elem: "#<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>"
                ,type: 'datetime'
            });
        })
    </script>
    <!-- 日期和时间 end -->
<?php break; ?>
            <!-- 扩展 end -->
        <?php endswitch; ?>
<?php echo isset($vo["ey_1563185380"])?$vo["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($vo["ey_1563185376"]))?$vo["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $vo = []; ?>
            <!-- 结束 -->
            <div class="ey-row">
                <input type="button" onclick="UpdateUsersData();" class="el-button el-button--primary wb100" value="保存资料"/>
            </div>
        </form>
    </div>


    <!-- 手机端 -->
    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/layer_mobile/layer.js","","",""); echo $__VALUE__; ?>
    <!-- 修改密码 -->
    <div id="users_change_pwd_html" style="display: none;">
        <div class="changepass">
            <form name='theForm_mobile_pwd' id="theForm_mobile_pwd" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="password" name="oldpassword" required class="form-control" placeholder="原密码">
                    </div>
                    <br/>
                    <div class="form-group">
                        <input type="password" name="password" required class="form-control" placeholder="新密码">
                    </div>
                    <br/>
                    <div class="form-group">
                        <input type="password" name="password2" required data-password="password" class="form-control" placeholder="确认密码">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="SubmitPwdData();">确定</button>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        var RootDir = '<?php echo $RootDir; ?>';
        // 提示信息，2秒自动关闭
        function msg_open(msgs){
            layer.open({
                content: msgs,
                skin: 'msg',
                time: 2,
            });
        }

        // 提示信息，估计在底部提示，点击空白处关闭
        function footer_open(msgs){
            layer.open({
                content: msgs,
                skin: 'footer',
            });
        }

        // 提示动画
        function loa_ding(){
            var loading = layer.open({
                type:2,
                content: '正在处理',
            });
            return loading;
        }

        // 修改密码
        function ChangePwdMobile()
        {
            var content = $('#users_change_pwd_html').html();
            content = content.replace(/theForm_mobile_pwd/g, 'change_pwd_mobile_2019');
            layer.open({
                type: 1,
                title: '修改密码',
                style:'position:fixed; bottom:0; left:0; width: 100%; padding:10px 0; border:none;max-width: 100%;',
                anim:'up',
                content: content,
            });
            
        }

        // 提交修改密码信息
        function SubmitPwdData()
        {
            var loading = loa_ding();// 正在处理提示动画
            $.ajax({
                url: "<?php echo url("user/Users/change_pwd","",true,false,null,null,null);?>",
                data: $('#change_pwd_mobile_2019').serialize(),
                type:'post',
                dataType:'json',
                success:function(res){
                    if(res.code == 1){
                        layer.closeAll();
                        msg_open(res.msg);
                    }else{
                        layer.close(loading);
                        msg_open(res.msg);
                    }
                },
                error : function(e) {
                    layer.close(loading);
                    footer_open(e.responseText);
                }
            });
        }

        // 修改会员属性信息
        function UpdateUsersData()
        {
            var loading = loa_ding();// 正在处理提示动画
            $.ajax({
                url: "<?php echo url("user/Users/centre_update","",true,false,null,null,null);?>",
                data: $('#theForm').serialize(),
                type:'post',
                dataType:'json',
                success:function(res){
                    layer.closeAll();
                    if (1 == res.code) {
                        // 删除密码框
                        // $('#password').parent().parent().remove();
                        layer.open({
                            content: res.msg,
                            skin: 'msg',
                            time: 1,
                            end: function () {
                                location.reload();
                            }
                        });
                    } else {
                        msg_open(res.msg);
                    }
                },
                error : function(e) {
                    layer.closeAll();
                    footer_open(e.responseText);
                }
            });
        };
    </script>
    <!-- 修改密码结束 -->

    <!-- 绑定、更换邮箱 -->
    <div id="users_bind_email_html" style="display: none;">
        <div class="el-row">
            <form name='theForm_mobile_email' id="theForm_mobile_email" method="post">
                <div class="ey-popup">
                     <div class="el-form-item">
                         <div class="el-input">
                             <input type="text" id="email_mobile_old" name="email" <?php if($users['is_email'] == '0'): ?> value="<?php echo $users['email']; ?>" <?php endif; ?> required class="el-input__inner" placeholder="新的邮箱地址">
                         </div>
                     </div>
                     <div class="el-form-item">
                         <div class="el-input el-input-group el-input-group--append el-input-group--prepend">
                             <input type="text" class="el-input__inner" id="email_code_mobile" name="email_code" placeholder="邮箱验证码" style="">
                              <div class="el-input-group__append">
                                   <input type="button" id="email_button_mobile" onclick="GetEmailCodeMobile();" class="el-button el-button--default" value="点击发送" />
                              </div>  
                         </div>
                     </div>
                     <div class="el-form-item">
                          <div class="el-input">
                               <button type="button" id="email_button_mobile" onclick="SubmitDataMobile();" class="el-button el-button--primary" style="width:86%;">确定</button>
                          </div>
                     </div>
                     
                 </div>
                
              <!-- <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" id="email_mobile_old" name="email" <?php if($users['is_email'] == '0'): ?> value="<?php echo $users['email']; ?>" <?php endif; ?> required class="form-control" placeholder="新的邮箱地址">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group" style="position: relative;">
                            <input type="text" class="form-control" id="email_code_mobile" name="email_code" placeholder="邮箱验证码" style="">
                            <input type="button" id="email_button_mobile" onclick="GetEmailCodeMobile();" class="btn btn-primary" value="点击发送" />
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn_zb" onclick="SubmitDataMobile();">确定</button>
                </div> -->
                
                
            </form>
        </div>
    </div>
        
    <script type="text/javascript">
        // 绑定、更换邮箱
        function BindEmailMobile(title)
        {
            var content = $('#users_bind_email_html').html();
            content = content.replace(/theForm_mobile_email/g, 'bind_email_mobile_2019');
            content = content.replace(/email_mobile_old/, 'email_mobile_2019');
            content = content.replace(/email_code_mobile/, 'email_code_mobile_2019');
            content = content.replace(/email_button_mobile/, 'email_button_mobile_2019');
            layer.open({
                type: 1,
                title: title,
                style:'position:fixed; bottom:0; left:0; width: 100%; padding:10px 0; border:none;max-width: 100%;',
                anim:'up',
                content: content,
            });
        }

        // 获取邮箱验证码
        function GetEmailCodeMobile()
        {
            // 正在处理提示动画
            var loading = loa_ding();
            // 标题
            var title = $('h3').html();
            // 邮箱地址
            var email = $("#email_mobile_2019").val();
            // 验证邮箱格式是否正确
            var reg = /^[a-z0-9]([a-z0-9\\.]*[-_]{0,4}?[a-z0-9-_\\.]+)*@([a-z0-9]*[-_]?[a-z0-9]+)+([\.][\w_-]+){1,5}$/i;
            if(!reg.test(email)){
                layer.close(loading);
                msg_open('邮箱格式不正确，请正确输入邮箱地址！');
                return false;
            }

            $("#email_button_mobile_2019").val('发送中…');
            $.ajax({
                url: "<?php echo url("user/Smtpmail/send_email","",true,false,null,null,null);?>",
                data: {email:email,title:title,type:'bind_email',scene:'3'},
                type:'post',
                dataType:'json',
                success:function(res){
                    if(res.code == 1){
                        layer.close(loading);
                        CountDown();
                        msg_open(res.msg);
                    }else{
                        $("#email_button_mobile_2019").val('点击发送');
                        layer.close(loading);
                        msg_open(res.msg);
                    }
                },
                error : function(e) {
                    $("#email_button_mobile_2019").val('点击发送');
                    layer.close(loading);
                    footer_open(e.responseText);
                }
            });
        }

        // 提交邮箱绑定信息
        function SubmitDataMobile()
        {   
            var loading = loa_ding();// 正在处理提示动画

            // 验证邮箱格式是否正确
            var email = $("#email_mobile_2019").val();
            var reg = /^[a-z0-9]([a-z0-9\\.]*[-_]{0,4}?[a-z0-9-_\\.]+)*@([a-z0-9]*[-_]?[a-z0-9]+)+([\.][\w_-]+){1,5}$/i;
            if(!reg.test(email)){
                layer.close(loading);
                msg_open('邮箱格式不正确，请正确输入邮箱地址！');
                return false;
            }

            // 邮箱验证码不能为空
            var email_code = $("#email_code_mobile_2019").val();
            if(!email_code){
                layer.close(loading);
                msg_open('邮箱验证码不能为空，请正确输入！');
                return false;
            }

            $.ajax({
                url: "<?php echo url("user/Users/bind_email","",true,false,null,null,null);?>",
                data: $('#bind_email_mobile_2019').serialize(),
                type:'post',
                dataType:'json',
                success:function(res){
                    if(res.code == 1){
                        layer.closeAll();
                        msg_open(res.msg);
                        location.reload();
                    }else{
                        layer.close(loading);
                        msg_open(res.msg);
                    }
                },
                error : function(e) {
                    layer.close(loading);
                    footer_open(e.responseText);
                }
            });
        };

        // 倒计时
        function CountDown(){
            var setTime;
            var time = <?php echo config('global.email_send_time'); ?>;
            setTime = setInterval(function(){
                if(0 >= time){
                    clearInterval(setTime);
                    return;
                }
                time--;
                $("#email_button_mobile_2019").val(time+'秒');
                $("#email_button_mobile_2019").attr('disabled', 'disabled');

                if(time == 0){
                    $("#email_button_mobile_2019").val('点击发送');
                    $("#email_button_mobile_2019").removeAttr("disabled");
                }
            },1000);
        };
    </script>
    <!-- 绑定、更换邮箱结束 -->
    <!-- 手机端结束 -->
