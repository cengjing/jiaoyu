
// 加载评论整体页面
function ajaxComment_1572491762(aid, root_dir)
{
    var before_display = document.getElementById('ey_comment').style.display;
    document.getElementById('ey_comment').style.display = 'none';
    var aid = parseInt(aid);
    //步骤一:创建异步对象
    var ajax = new XMLHttpRequest();
    //步骤二:设置请求的url参数,参数一是请求的类型,参数二是请求的url,可以带参数,动态的传递参数starName到服务端
    ajax.open("get", root_dir+'/index.php?m=plugins&c=Comment&a=ajax_comment&aid='+aid+'&_ajax=1', true);
    // 给头部添加ajax信息
    ajax.setRequestHeader("X-Requested-With","XMLHttpRequest");
    //步骤三:发送请求
    ajax.send();
    //步骤四:注册事件 onreadystatechange 状态改变就会调用
    ajax.onreadystatechange = function () {
        //步骤五 如果能够进到这个判断 说明 数据 完美的回来了,并且请求的页面是存在的
        if (ajax.readyState==4 && ajax.status==200) {
            var json = ajax.responseText;  
            var res = JSON.parse(json);
            if (1 == res.code) {
                try{
        　　　　    document.getElementById('ey_comment').innerHTML = res.data.html;
                    if (!before_display) {
                        document.getElementById('ey_comment').style.display = before_display;
                    }
                    var plugins = res.data.plugins;
                    if (1 == plugins.isComment) {
                        showEditor_1572491762(root_dir,plugins.isLogin);
                    }
                }catch(e){}
            } else {
                if (0 == res.data.isStatus) {
                    document.getElementById('ey_comment').innerHTML = '';
                    document.getElementById('ey_comment').style.display = 'none';
                }
            }
      　} else {
            try {
                if (!before_display) {
                    document.getElementById('ey_comment').style.display = before_display;
                }
                document.getElementById('ey_comment').innerHTML = '加载失败~';
            }
            catch(err){}
        }
    }
}

// 渲染编辑器
function showEditor_1572491762(root_dir,isLogin){
    isLogin = parseInt(isLogin);
    if (isLogin > 0) {
        var toolbars = [["forecolor", "backcolor", "removeformat", "|", "simpleupload", "unlink"]];
        var serverUrl = root_dir+'/index.php?m=user&c=Uploadify&a=index&savepath=weapp_comment';
    } else {
        var toolbars = [["forecolor", "backcolor", "removeformat"]];
        var serverUrl = '';
    }
    UE.getEditor('plugins_comment_content',{
        serverUrl : serverUrl,
        zIndex: 999,
        initialFrameWidth: "100%", //初化宽度
        initialFrameHeight: 300, //初化高度            
        focus: false, //初始化时，是否让编辑器获得焦点true或false
        maximumWords: 99999,
        removeFormatAttributes: 'class,style,lang,width,height,align,hspace,valign',//允许的最大字符数 'fullscreen',
        pasteplain:false, //是否默认为纯文本粘贴。false为不使用纯文本粘贴，true为使用纯文本粘贴
        autoHeightEnabled: false,
        toolbars: toolbars,
        // xss 过滤是否开启,inserthtml等操作
        xssFilterRules: true,
        //input xss过滤
        inputXssFilter: true,
        //output xss过滤
        outputXssFilter: true
    });
}

// 添加评论回复
function CommentAdd(obj, url){
    var plugins_comment_content = jQuery('textarea[name="plugins_comment_content"]').val();
    plugins_comment_content = jQuery.trim(plugins_comment_content);
    if (!plugins_comment_content) {
        UE.getEditor('plugins_comment_content').focus();
        layer.msg('请写下您的评论！', {time: 1500, icon: 5});
        return false;
    }

    layer_loading('正在处理');
    jQuery.ajax({
        url: url,
        data: jQuery('#commentForm').serialize(),
        type:'post',
        dataType:'json',
        success:function(res){
            layer.closeAll();
            if (1 == res.code) {
                if (res.data.review) {
                    var times = 2500;
                }else{
                    var times = 1000;
                }
                layer.msg(res.msg, {time: times},function(){
                    window.location.reload();
                });
            } else {
                layer.msg(res.msg, {time: 1500, icon: 5});
            }
        },
        error : function() {
            layer.closeAll();
            layer.alert('未知错误，无法继续~', {icon: 5});
        }
    });
}
function CommentAdds(obj, url){

    layer_loading('正在处理');
    jQuery.ajax({
        url: url,
        data: jQuery('#commentForm').serialize(),
        type:'post',
        dataType:'json',
        success:function(res){
            layer.closeAll();
            if (1 == res.code) {
                if (res.data.review) {
                    var times = 2500;
                }else{
                    var times = 1000;
                }
                layer.msg(res.msg, {time: times},function(){
                    window.location.reload();
                });
            } else {
                layer.msg(res.msg, {time: 1500, icon: 5});
            }
        },
        error : function() {
            layer.closeAll();
            layer.alert('未知错误，无法继续~', {icon: 5});
        }
    });
}
// 编辑评论回复
function CommentEdit(url, comment_id) {
    if (url.indexOf('?') > -1) {
        url += '&';
    } else {
        url += '?';
    }
    url += 'comment_id='+comment_id;

    //iframe窗
    layer.open({
        type: 2,
        title: '编辑评论',
        area: ['60%', '90%'],
        content: url
    });
}

// 编辑提交评论
function SubmitData(url) {
    var plugins_comment_content = jQuery('textarea[name="plugins_comment_content"]').val();
    plugins_comment_content = jQuery.trim(plugins_comment_content);
    if (!plugins_comment_content) {
        UE.getEditor('plugins_comment_content').focus();
        layer.msg('请写下您的评论！', {time: 1500, icon: 5});
        return false;
    }
    var parentObj = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
    var _parent = parent;

    if (url.indexOf('?') > -1) {
        url += '&';
    } else {
        url += '?';
    }
    url += '_ajax=1';

    layer_loading('正在处理');
    jQuery.ajax({
        url: url,
        data: jQuery('#CommentFormData').serialize(),
        type:'post',
        dataType:'json',
        success:function(res){
            layer.closeAll();
            if (1 == res.code) {
                parent.layer.close(parentObj);
                parent.layer.msg(res.msg, {time: 1000}, function(){
                    _parent.window.location.reload();
                });
            } else {
                layer.msg(res.msg, {time: 1500, icon: 5});
            }
        },
        error:function() {
            layer.closeAll();
            layer.alert('未知错误，无法继续~', {icon: 5});
        }
    });
}

// 管理员审核评论
function Review(url, comment_id) {
    layer.confirm('确认审核该评论？', {
        btn: ['确认', '取消'],
        title: false,
        closeBtn: false
    }, function () {
        layer_loading('正在处理');
        jQuery.ajax({
            url: url,
            data: {comment_id:comment_id},
            type:'post',
            dataType:'json',
            success:function(res){
                layer.closeAll();
                if (1 == res.code) {
                    layer.msg(res.msg, {time: 1500}, function(){
                        jQuery('#'+comment_id+'_Review').remove();
                    });
                }else{
                    layer.msg(res.msg, {time: 2000, icon: 5});
                }
            },
            error:function() {
                layer.closeAll();
                layer.alert('未知错误，无法继续~', {icon: 5});
            }
        });
    });
}

// 删除该整个评论
function CommentDel(url, id, type) {
    if(id <= 0){
        var msg = "没有选择评论！";
    }else if(type == 1){
        var msg = "你确定要删除该评论？";
    }else if(type == 2){
        // 删除评论回复
        ReplyDel(url, id);
        return false;
    }
    layer.confirm(msg, {
        btn: ['确认', '取消'],
        title: false,
        closeBtn: false,
    }, function () {
        layer_loading('正在处理');
        jQuery.ajax({
            url: url,
            data: {id:id, type:type},
            type:'post',
            dataType:'json',
            success:function(res){
                layer.closeAll();
                if (1 == res.code) {
                    layer.msg(res.msg, {time: 1500});
                    if (1 == type) {
                        jQuery('#ul_div_li_'+id).remove();
                    }
                }else{
                    layer.msg(res.msg, {time: 2000, icon:5});
                }
            },
            error:function() {
                layer.closeAll();
                layer.alert('未知错误，无法继续~', {icon: 5});
            }
        });
    });
}

// 删除回复
function ReplyDel(url, comment_id) {
    if (!comment_id) layer.msg('请选择删除信息！', {time: 1500, icon:5});
    layer_loading('正在处理');
    jQuery.ajax({
        url:  url,
        type: 'POST',
        dataType: 'json',
        data: {comment_id:comment_id},
        success: function(res){
            layer.closeAll();
            if (1 == res.code) {
                jQuery('#'+comment_id+'_comment_li').remove();
                layer.msg(res.msg, {time: 1500});
            } else {
                layer.msg(res.msg, {time: 1500, icon:5});
            }
        },
        error: function(e){
            layer.closeAll();
            layer.msg('未知错误，无法继续~', {time: 1500, icon:5});
        }
    });
}

// 回复评论
function Reply(comment_id, url){
    var icontentObj = jQuery('#'+comment_id+'_i_contentInput');
    if(!jQuery.trim(icontentObj.val())){
        jQuery("#"+comment_id+"_errorMsg").html('请写下您的回复！');
        //输入框内容改变时,清空错误提示
        icontentObj.bind('input propertychange', function() {
            jQuery("#"+comment_id+"_errorMsg").html("");
        })
        return false;
    }

    //提交服务器
    layer_loading('正在处理');
    jQuery.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: jQuery('#'+comment_id+'_replyForm').serialize(),
        success: function(res){
            layer.closeAll();
            if (1 == res.code) {
                /*提示及追加html处理*/
                var times = res.data.review ? 2000 : 1000;
                if (res.data.htmlcode) jQuery("#"+comment_id+"_ReplyContainer").append(res.data.htmlcode);
                UnReplyUser(comment_id);
                layer.msg(res.msg, {time: times});
                /* END */
            } else {
                layer.msg(res.msg, {time: 1500, icon: 5});
            }
        },
        error : function() {
            layer.closeAll();
            layer.alert('未知错误，无法继续~', {icon: 5});
        }
    });
}

// @回复用户
function ReplyUser(comment_id, at_users_id, at_username, at_comment_id){
    jQuery("#"+comment_id+"_at_users_id").val(at_users_id);
    jQuery("#"+comment_id+"_at_comment_id").val(at_comment_id);

    var showObj = jQuery("#"+comment_id+"_contentInput");
    var inputObj = jQuery("#"+comment_id+"_i_contentInput");
    showObj.attr("placeholder","回复 "+at_username+ "：");
    inputObj.attr("placeholder","回复 "+at_username+ "：");
    inputObj.val("");
    showObj.val("");

    var showContainer =  jQuery("#"+comment_id+"_init_ta");
    showContainer.hide();
    var inputContainer = jQuery("#"+comment_id+"_ta").val("");
    inputContainer.show();
    inputObj.focus();
}

// 取消@回复用户
function UnReplyUser(comment_id){
    jQuery("#"+comment_id+"_at_users_id").val("");
    jQuery("#"+comment_id+"_at_comment_id").val("");

    var showObj = jQuery("#"+comment_id+"_contentInput");
    var inputObj = jQuery("#"+comment_id+"_i_contentInput");
    showObj.attr("placeholder",showObj.attr("defTips"));
    inputObj.attr("placeholder",inputObj.attr("defTips"));
    inputObj.val("");
    showObj.val("");

    jQuery("#"+comment_id+"_init_ta").show();
    jQuery("#"+comment_id+"_ta").val("").hide();
}

// 点赞
function ClickLike(obj, aid, comment_id) {
    if (false == !jQuery(obj).attr('data-is_like')) {
        layer.msg('您已赞过！', {time: 1500});
        return false;
    }
    jQuery.ajax({
        url: jQuery(obj).data('url'),
        type: 'POST',
        dataType: 'json',
        data: {aid:aid, comment_id:comment_id},
        success: function(res) {
            if (1 == res.code) {
                if (3 < res.data.LikeCount) {
                    jQuery('#click_like_span_' + comment_id).children().eq(2).remove();
                }
                // 追加点赞人
                jQuery("#click_like_name_" + comment_id).prepend(res.data.LikeName);
                // 点赞次数
                jQuery('#click_like_' + comment_id).html(res.data.LikeCount);
                // 显示点赞框
                jQuery('#is_show_' + comment_id).show();
                jQuery('#askanswer_click_like_' + comment_id).html(res.data.LikeCount);
                // 设置当前用户已点赞过，用户再次点击则不需要执行ajax
                jQuery(obj).attr('data-is_like', true);
            } else {
                layer.msg(res.msg, {time: 1500});
            }
        },
        error : function() {
            layer.closeAll();
            layer.alert('未知错误！', {icon: 5});
        }
    });
}

// 点击时加载评论框
function initReply(comment_id){
    jQuery("#"+comment_id+"_init_ta").hide();
    jQuery("#"+comment_id+"_ta").val("").show();
    jQuery("#"+comment_id+"_ta #"+comment_id+"_i_contentInput").focus();//设置光标位置
    jQuery("#"+comment_id+"_ta #"+comment_id+"_errorMsg").html("");//隐藏回复错误提示
}

function focusTextArea(obj){
    _obj = jQuery(obj);
    if(_obj.val()==_obj.attr("defTips")){
        _obj.val("");
    }
}

function blurTextArea(obj){
    _obj = jQuery(obj);
    if(_obj.val()==''){
        _obj.attr("placeholder",_obj.attr("defTips"));
    }else{
        dealInputContentAndSize(obj);
    }
}

// 对输入框限制的内容与字数处理
function dealInputContentAndSize(obj){
    var _obj = jQuery(obj);
    str = _obj.val();
    var maxLength=_obj.attr("maxlength");
    
    var returnValue = ''; 
    var count = 0; 
    var temp = 0;
    for (var i = 0; i < str.length; i++) { 
        count += 1; 
        temp = 1;
        if (count > maxLength) {
            count -= temp;
            break; 
        }
        returnValue += str[i]; 
    } 
    _obj.val(returnValue);
}

// 采纳最佳评论
function BestAnswer(obj, comment_id) {
    if (!comment_id) layer.msg('请选择采纳的回答！', {time: 1500, icon: 2});
    //提交服务器
    layer_loading('正在处理');
    jQuery.ajax({
        url: jQuery(obj).data('url'),
        type: 'post',
        dataType: 'json',
        data: {comment_id:comment_id},
        success: function(res){
            layer.closeAll();
            if (1 == res.code) {
                layer.msg(res.msg, {time: 1000},function(){
                    window.location.reload();
                });
            } else {
                layer.msg(res.msg, {time: 1500, icon: 2});
            }
        },
        error : function() {
            layer.closeAll();
            layer.alert('未知错误！', {icon: 5});
        }
    });
}

// 加载层
function layer_loading(msg){
    var loading = layer.msg(
    msg+'...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;请勿刷新页面', 
    {
        icon: 1,
        time: 3600000, //1小时后后自动关闭
        shade: [0.2] //0.1透明度的白色背景
    });
    //loading层
    var index = layer.load(3, {
        shade: [0.1,'#fff'] //0.1透明度的白色背景
    });

    return loading;
}

// 获取指定数量的评论数据（ajax加载更多）
function pageComment(obj) {
    /*处理查询数据*/
    var firstRow = jQuery(obj).attr('data-firstRow');
    var listRows = jQuery(obj).attr('data-listRows');
    firstRow = parseInt(firstRow) + parseInt(listRows);
    /* END */

    //提交服务器
    layer_loading('正在处理');
    jQuery.ajax({
        url: jQuery(obj).data('url'),
        type: 'post',
        dataType: 'json',
        data: {firstRow:firstRow},
        success: function(res){
            layer.closeAll();
            if (1 == res.code) {
                // 追加html处理
                if (res.data.html) {
                    jQuery("#commentContainerDiv").append(res.data.html);
                    // 更新下一次提交查询数量
                    jQuery(obj).attr('data-firstRow', firstRow);
                } else {
                    layer.msg('没有数据了', {time: 1500});
                }
                // 最后一页，没有数据了
                var plugins = res.data.plugins;
                if (plugins.comment.commentMore == 0) {
                    jQuery('#comment_morepage').hide();
                }
            } else {
                layer.msg(res.msg, {time: 1500, icon: 5});
            }
        },
        error : function() {
            layer.closeAll();
            layer.alert('未知错误，无法继续~', {icon: 5});
        }
    });
}

// 获取指定数量的回复数据（ajax加载更多）
function pageReply(obj, comment_id) {
    /*处理查询数据*/
    var firstRow = jQuery(obj).attr('data-firstRow');
    var replylistRows = jQuery(obj).attr('data-replylistRows');
    firstRow = parseInt(firstRow) + parseInt(replylistRows);
    /* END */

    //提交服务器
    layer_loading('正在处理');
    jQuery.ajax({
        url: jQuery(obj).data('url'),
        type: 'post',
        dataType: 'json',
        data: {comment_id:comment_id,firstRow:firstRow},
        success: function(res){
            layer.closeAll();
            if (1 == res.code) {
                // 追加html处理
                if (res.data.htmlcode) jQuery("#"+comment_id+"_ReplyContainer").append(res.data.htmlcode);
                // 更新下一次提交查询数量
                jQuery(obj).attr('data-firstRow', firstRow);
                // 最后一页，没有数据了
                if (res.data.ismore == 0) {
                    jQuery('#'+comment_id+'_movepage').hide();
                }
            } else {
                layer.msg(res.msg, {time: 1500});
            }
        },
        error : function() {
            layer.closeAll();
            layer.alert('未知错误，无法继续~', {icon: 5});
        }
    });
}

// 点赞排序
function commentLike(obj, root_dir) {
    var sort_order = jQuery(obj).data('sort_order');
    var url = jQuery(obj).data('url');
    if (url.indexOf('?') > -1) {
        url += '&';
    } else {
        url += '?';
    }
    url += 'click_like=' + sort_order;

    layer_loading('正在处理');
    jQuery.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {},
        success: function(res){
            layer.closeAll();
            if (1 == res.code) {
                jQuery('#ey_comment').html(res.data.html);
                try{
                    var plugins = res.data.plugins;
                    if (1 == plugins.isComment) {
                        showEditor_1572491762(root_dir,plugins.isLogin);
                    }
                }catch(e){}
            } else {
                jQuery('#ey_comment').html('加载失败~');
            }
        },
        error : function() {
            layer.closeAll();
            layer.alert('未知错误，无法继续~', {icon: 5});
        }
    });    
}