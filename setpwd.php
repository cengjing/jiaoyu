<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>密码修改工具 </title>
</head>
<body>
<?php
    error_reporting(E_ALL & ~E_NOTICE);
    header('Content-Type: text/html; charset=UTF-8');
    // 数据绝对路径
    defined('DATA_PATH') or define('DATA_PATH', __DIR__ . '/data/');
    // 运行缓存
    defined('RUNTIME_PATH') or define('RUNTIME_PATH', DATA_PATH . 'runtime/');
    // 安装程序定义
    defined('DEFAULT_INSTALL_DATE') or define('DEFAULT_INSTALL_DATE',1525756440);
    // 序列号
    defined('DEFAULT_SERIALNUMBER') or define('DEFAULT_SERIALNUMBER','20180508131400oCWIoa');
    // 定义应用目录
    defined('APP_PATH') or define('APP_PATH', __DIR__ . '/application/');
    defined('EXT') or define('EXT', '.php');
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);
    defined('THINK_PATH') or define('THINK_PATH', __DIR__ . DS);
    defined('CACHE_PATH') or define('CACHE_PATH', RUNTIME_PATH . 'cache' . DS);
    defined('ROOT_PATH') or define('ROOT_PATH', dirname(realpath(APP_PATH)) . DS);

    $version = getCmsVersion(); // 系统版本号
    $database = include_once "application/database.php";
    $dbHost = trim($database['hostname']);
    $dbport = $database['hostport'] ? $database['hostport'] : '3306';
    $dbName = trim($database['database']);
    $dbUser = trim($database['username']);
    $dbPwd = trim($database['password']);
    $dbPrefix = empty($database['prefix']) ? 'ey_' : trim($database['prefix']);
    $charset = trim($database['charset']);

    $conn = @mysqli_connect($dbHost, $dbUser, $dbPwd,$dbName,$dbport);
    if (mysqli_connect_error()){
        $msg = "连接数据库失败!".mysqli_connect_error($conn);
        tips($msg);
    }
    mysqli_set_charset($conn, $charset);

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        $type = $_POST['type'];
        if (1 == $type) { // 修改后台登录密码
            $user_name = $_POST['user_name'];
            $password = $_POST['password'];
            if (empty($password)) {
                tips("新密码不能为空！");
            }

            $config = include_once "application/config.php";
            $auth_code = $config['AUTH_CODE'];
            $sql = "SELECT value FROM `{$dbPrefix}config` WHERE name = 'system_auth_code' AND inc_type='system' LIMIT 1";
            $ret = mysqli_query($conn,$sql);
            while($row = mysqli_fetch_array($ret))
            {
                if (!empty($row['value'])) {
                    $auth_code = $row['value'];
                }
            }

            if (version_compare($version,'v1.5.7','<')) {
                $password = md5($auth_code.$password);
            } else {
                $entry = pwd_encry_type('bcrypt');
                if ('bcrypt' == $entry) {
                    $crypt_auth_code = '';
                    $sql = "SELECT value FROM `{$dbPrefix}config` WHERE name = 'system_crypt_auth_code' AND inc_type='system' LIMIT 1";
                    $ret = mysqli_query($conn,$sql);
                    while($row = mysqli_fetch_array($ret))
                    {
                        if (!empty($row['value'])) {
                            $crypt_auth_code = $row['value'];
                        }
                    }
                    if (!empty($crypt_auth_code)) {
                        $password = crypt($password, $crypt_auth_code);
                    } else {
                        $password = md5($auth_code.$password);
                    }
                } else {
                    $password = md5($auth_code.$password);
                }
            }

            $sql = "UPDATE `{$dbPrefix}admin` SET `password`='{$password}' WHERE `user_name`='{$user_name}'";
            $ret = mysqli_query($conn,$sql);
            if ($ret) {
                $msg = "修改成功";
            } else {
                $msg = "修改失败！";
            }

            tips($msg);
        }
        else if (2 == $type) // 修改插件安装密码
        {
            $password = $_POST['password'];
            if (empty($password)) {
                tips("新密码不能为空！");
            }

            $config = include_once "application/config.php";
            $auth_code = $config['AUTH_CODE'];
            $sql = "SELECT value FROM `{$dbPrefix}config` WHERE name='system_auth_code' AND inc_type='system' LIMIT 1";
            $ret = mysqli_query($conn,$sql);
            while($row = mysqli_fetch_array($ret))
            {
                if (!empty($row['value'])) {
                    $auth_code = $row['value'];
                }
            }
            $password = md5($auth_code.$password);

            $t = time();
            $sql = "UPDATE `{$dbPrefix}config` SET `value`='{$password}', `update_time`='{$t}' WHERE `name`='weapp_installpwd' AND `inc_type`='weapp'";
            $ret = mysqli_query($conn,$sql);
            if ($ret) {
                delFile('./data/runtime/cache', true);
                $msg = "修改成功";
            } else {
                $msg = "修改失败！";
            }

            tips($msg);
        }
        else if (3 == $type) // 修改插件问题与答案
        {
            $ask = $_POST['ask'];
            if (empty($ask)) {
                tips("新问题不能为空！");
            }

            $answer = $_POST['answer'];
            if (empty($answer)) {
                tips("新答案不能为空！");
            }

            $config = include_once "application/config.php";
            $auth_code = $config['AUTH_CODE'];
            $sql = "SELECT value FROM `{$dbPrefix}config` WHERE name='system_auth_code' AND inc_type='system' LIMIT 1";
            $ret = mysqli_query($conn,$sql);
            while($row = mysqli_fetch_array($ret))
            {
                if (!empty($row['value'])) {
                    $auth_code = $row['value'];
                }
            }
            $answer = md5($auth_code.$answer);

            $t = time();
            $sql = "UPDATE `{$dbPrefix}setting` SET `value`='{$ask}', `update_time`='{$t}' WHERE `name`='weapp_install_ask' AND `inc_type`='weapp'";
            $sql2 = "UPDATE `{$dbPrefix}setting` SET `value`='{$answer}', `update_time`='{$t}' WHERE `name`='weapp_install_answer' AND `inc_type`='weapp'";
            if (@mysqli_query($conn,$sql) && @mysqli_query($conn,$sql2)) {
                delFile('./data/runtime/cache', true);
                $msg = "修改成功";
            } else {
                $msg = "修改失败！";
            }

            tips($msg);
        }
        else if (4 == $type) // 修改二次安全验证的答案
        {
            $answer = trim($_POST['answer']);
            if (empty($answer)) {
                tips("新答案不能为空！");
            }

            $config = include_once "application/config.php";
            $auth_code = $config['AUTH_CODE'];
            $sql = "SELECT value FROM `{$dbPrefix}config` WHERE name='system_auth_code' AND inc_type='system' LIMIT 1";
            $ret = mysqli_query($conn,$sql);
            while($row = mysqli_fetch_array($ret))
            {
                if (!empty($row['value'])) {
                    $auth_code = $row['value'];
                }
            }

            $entry = pwd_encry_type('bcrypt');
            if ('bcrypt' == $entry) {
                $crypt_auth_code = '';
                $sql = "SELECT value FROM `{$dbPrefix}config` WHERE name = 'system_crypt_auth_code' AND inc_type='system' LIMIT 1";
                $ret = mysqli_query($conn,$sql);
                while($row = mysqli_fetch_array($ret))
                {
                    if (!empty($row['value'])) {
                        $crypt_auth_code = $row['value'];
                    }
                }
                if (!empty($crypt_auth_code)) {
                    $answer = crypt($answer, $crypt_auth_code);
                } else {
                    $answer = md5($auth_code.$answer);
                }
            } else {
                $answer = md5($auth_code.$answer);
            }

            $t = time();
            $sql1 = "UPDATE `{$dbPrefix}setting` SET `value`='{$answer}', `update_time`='{$t}' WHERE `name`='security_answer' AND `inc_type`='security'";
            if (@mysqli_query($conn,$sql1)) {
                delFile('./data/runtime/cache', true);
                $msg = "修改成功";
            } else {
                $msg = "修改失败！";
            }

            tips($msg);
        }

    } else {
        /*管理员列表*/
        $sql = "SELECT * FROM `{$dbPrefix}admin`";
        $ret = mysqli_query($conn,$sql);
        $select_html = "";
        while($row = mysqli_fetch_array($ret))
        {
            $select_html .= "<option value='{$row['user_name']}'>{$row['user_name']}</option>";
        }
        /*end*/

        /*插件问题列表*/
        $weapp_askanswer_list = [];
        $sql = "SELECT value FROM `{$dbPrefix}setting` WHERE name='weapp_askanswer_list' AND inc_type='weapp' LIMIT 1";
        $ret = mysqli_query($conn,$sql);
        while($row = mysqli_fetch_array($ret))
        {
            if (!empty($row['value'])) {
                $weapp_askanswer_list = json_decode($row['value'], true);
            }
        }
        $ask_select_html = "";
        foreach ($weapp_askanswer_list as $key => $value) {
            $ask_select_html .= "<option value='{$key}'>{$value}</option>";
        }
        /*end*/

        /*二次安全验证的问题名称*/
        $security_ask = '';
        $sql = "SELECT value FROM `{$dbPrefix}setting` WHERE name = 'security_ask' AND inc_type='security' LIMIT 1";
        $ret = mysqli_query($conn,$sql);
        while($row = mysqli_fetch_array($ret))
        {
            if (!empty($row['value'])) {
                $security_ask = $row['value'];
            }
        }
        /*end*/
    }
    mysqli_close($conn);

    function tips($msg)
    {
        die('<script type="text/javascript">alert("'.$msg.'");window.location.href = "setpwd.php";</script>');
    }

    /**
     * 获取CMS的版本号
     * @return [type] [description]
     */
    function getCmsVersion()
    {
        $version = 'v1.0.0';
        $version_txt_path = './data/conf/version.txt';
        if(file_exists($version_txt_path)) {
            $fp = fopen($version_txt_path, 'r');
            $content = fread($fp, filesize($version_txt_path));
            fclose($fp);
            $version = $content ? $content : $version;
        }
        return $version;
    }

    /**
     * 获取密码加密方式
     * @param  string $encry_pwd 
     * @return [type]            [description]
     */
    function pwd_encry_type($encry_pwd = '') {
        $entry = 'md5';
        if (32 != strlen($encry_pwd)) {
            if (defined('CRYPT_BLOWFISH') && CRYPT_BLOWFISH == 1) {
                $entry = 'bcrypt';
            }
        }

        return $entry;
    }

    /**
     * 递归删除文件夹
     *
     * @param string $path 目录路径
     * @param boolean $delDir 是否删除空目录
     * @return boolean
     */
    function delFile($path, $delDir = FALSE) {
        if(!is_dir($path))
            return FALSE;       
        $handle = @opendir($path);
        if ($handle) {
            while (false !== ( $item = readdir($handle) )) {
                if ($item != "." && $item != "..")
                    is_dir("$path/$item") ? delFile("$path/$item", $delDir) : @unlink("$path/$item");
            }
            closedir($handle);
            if ($delDir) {
                return @rmdir($path);
            }
        }else {
            if (file_exists($path)) {
                return @unlink($path);
            } else {
                return FALSE;
            }
        }
    }
?>
<br/>
<font style="color: red; font-size: 18px;">注意：修改完之后，建议删掉该文件，免得被其他人修改入侵！</font>
<br/><br/><br/>
后台登录密码<br/><br/>
<form action="setpwd.php" method="post">
    用户名：
    <select name="user_name">
        <?php echo $select_html;?>
    </select><br/><br/>
    新密码：<input type="password" name="password" value=""><br/><br/>
    <input type="hidden" name="type" value="1">
    <input type="submit" name="submit" value="确认修改">
</form>

<?php if (version_compare($version,'v1.4.7','<')) { ?>
    <?php if (version_compare($version,'v1.4.2','>')) { ?>
    <br/>
    <hr/>
    <br/>
    易优CMS - 插件问题与答案<br/><br/>
    <form action="setpwd.php" method="post">
        新问题：
        <select name="ask">
            <?php echo $ask_select_html;?>
        </select><br/><br/>
        新答案：<input type="text" name="answer" value=""><br/><br/>
        <input type="hidden" name="type" value="3">
        <input type="submit" name="submit" value="确认修改">
    </form>
    <?php } else { ?>
    <br/>
    <hr/>
    <br/>
    插件安装密码<br/><br/>
    <form action="setpwd.php" method="post">
        新密码：<input type="password" name="password" value=""><br/><br/>
        <input type="hidden" name="type" value="2">
        <input type="submit" name="submit" value="确认修改">
    </form>
    <?php } ?>
<?php } else if (version_compare($version,'v1.5.6','>')) { ?>
    <br/>
    <hr/>
    <br/>
    二次安全验证<br/><br/>
    <form action="setpwd.php" method="post">
        问题名称：<?php echo $security_ask;?><br/><br/>
        新的答案：<input type="text" name="answer" value=""><br/><br/>
        <input type="hidden" name="type" value="4">
        <input type="submit" name="submit" value="确认修改">
    </form>
<?php } ?>

</body>
</html>